Intro
=====
SyntheticaAddons provides additional components and UI-delegates for Swing and
the SwingX project. The provided components are either completely build from
the scratch or based on SwingX. Each component comes along with its own look 
for each available Synthetica theme.

Home Page
=========
General:        http://www.jyloo.com
Synthetica:     http://www.jyloo.com/syntheticaaddons
FAQ:            http://www.jyloo.com/syntheticaaddons/faq
Screenshots:    http://www.jyloo.com/syntheticaaddons/screenshots
Download:       http://www.jyloo.com/syntheticaaddons/download
License:        http://www.jyloo.com/syntheticaaddons/license

Contact Addresses
=================
General:        info@jyloo.com
Sales:          sales@jyloo.com	
Support:        support@jyloo.com

System Requirements
===================
Java SE 5 (JRE 1.5.0) or later
Synthetica V2.19 or above
SwingX library (part of SyntheticaAddons, SwingX 1.6 requires Java 6 or above)
SwingWorker (part of SyntheticaAddons, Java 1.5 only)

SyntheticaAddons Libraries
==========================
syntheticaAddons.jar - core library
jywidgets.jar - additional components like JYButton, JYLabel, JYTabbedPane
jydocking.jar - JYDocking framework
synthetica{ThemeName}Addon.jar - theme extension 
syntheticaAddonsWithThemes.jar - optional core library including theme extensions 

SwingX Libraries
================
swingx-1.0.X.jar - SwingX core library for Java 1.5
swingx-1.6.X.jar - SwingX core library, Java 1.6 required
swing-worker-1.1.jar - Swing worker library, only required for Java 1.5

JIDE Support
============
JIDE provides a library called jide-plaf.jar - for proper support of JIDE components
please make sure that this lib is on your classpath too. 

NetBeans Support
================
SyntheticaAddons comes along with a NetBeans Plugin (see NetBeans folder) to support 
NetBeans specific components. For installation and usage with RCP see links below.
http://www.jyloo.com/news/?pubId=1308209905000
http://www.jyloo.com/news/?pubId=1322244511000

License
===========
Because Synthetica makes use of SwingX it is necessary to
deliver the license text file of this library with your application.

