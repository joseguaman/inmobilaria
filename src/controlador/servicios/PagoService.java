/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.servicios;

import controlador.daos.PagoDao;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import modelo.Pago;

/**
 *
 * @author sissysebas
 */
public class PagoService {
    private PagoDao obj = new PagoDao();
    public Pago getPago() {
        return obj.getPago();
    }
    
    public boolean guardar() {
        return obj.guardar();
    }
    
    public List<Pago> todos() {
        return obj.listar();
    }
    
    public Pago obtener(Long id) {
        return obj.obtener(id);
    }
    
    public void fijarPago(Pago pago) {
        obj.setPago(pago);
    }
    
    public List<Pago> listarLikePago(String texto) {
        return obj.listarLikePago(texto);
    }
    
    public List<Pago> listarPagoEstado(Boolean estado) {
        return obj.listarPagoEstado(estado);
    }
    
    public List<Pago> listarPagoClienteEstado(Boolean estado, String external_cliente) {
        return obj.listarPagoClienteEstado(estado, external_cliente);
    }
    
    public List<Pago> listarPagosPagadosInmueble(Boolean estado, String codigo, Date fi, Date ff) {
        return obj.listarPagosPagadosInmueble(estado, codigo, fi, ff);
    }
            
}