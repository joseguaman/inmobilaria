/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.servicios;

import controlador.daos.ContratoDao;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import modelo.Contrato;

/**
 *
 * @author sissysebas
 */
public class ContratoService {
    private ContratoDao obj = new ContratoDao();
    public Contrato getContrato() {
        return obj.getContrato();
    }
    
    public boolean guardar() {
        return obj.guardar();
    }
    
    public List<Contrato> todos() {
        return obj.listar();
    }
    
    public Contrato obtener(Long id) {
        return obj.obtener(id);
    }
    
    public void fijarContrato(Contrato contrato) {
        obj.setContrato(contrato);
    }
    
    public List<Contrato> listarLikeContrato(String texto) {
        return obj.listarLikeContrato(texto);
    }    
    
    public List<Contrato> listarContratoActivos() {
        return obj.listarContratoActivos();
    }
    
    public List<Contrato> listarContratoDesactivos() {
        return obj.listarContratoDesactivos();
    }
}