/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.servicios;

import controlador.daos.CuentaDao;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import modelo.Cuenta;

/**
 *
 * @author sissysebas
 */
public class CuentaService {
    private CuentaDao obj = new CuentaDao();
    public Cuenta getCuenta() {
        return obj.getCuenta();
    }
    
    public boolean guardar() {
        return obj.guardar();
    }
    
    public List<Cuenta> todos() {
        return obj.listar();
    }
    
    public Cuenta obtener(Long id) {
        return obj.obtener(id);
    }
    
    public void fijarCuenta(Cuenta cuenta) {
        obj.setCuenta(cuenta);
    }
    
    public void crearCuentaAdmin() {
        if(todos().isEmpty()) {
            PersonaService persona = new PersonaService();
            persona.getPersona().setApellidos("Maita");
            persona.getPersona().setNombres("Sergio");
            persona.getPersona().setCedula("2222222222");
            persona.getPersona().setDireccion("Loja");
            persona.getPersona().setExternal_id(UUID.randomUUID().toString());
            persona.getPersona().setTelefono("S/T");
            persona.getPersona().setRol(new RolService().buscarRolNombre("Administrador"));
            Cuenta c = new Cuenta();
            c.setClave("admin");
            c.setUsuario("admin");
            c.setExternal_id(UUID.randomUUID().toString());
            c.setCreated_at(new Date());
            c.setUpdated_at(new Date());
            c.setPersona(persona.getPersona());
            persona.getPersona().setCuenta(c);
            persona.guardar();          
            persona.fijarPersona(null);
            persona.getPersona().setApellidos("Secretario");
            persona.getPersona().setNombres("Secretario");
            persona.getPersona().setCedula("1111111111");
            persona.getPersona().setDireccion("Loja");
            persona.getPersona().setExternal_id(UUID.randomUUID().toString());
            persona.getPersona().setTelefono("S/T");
            persona.getPersona().setRol(new RolService().buscarRolNombre("Secretario"));
            Cuenta c1 = new Cuenta();
            c1.setClave("secretario");
            c1.setUsuario("secretario");
            c1.setExternal_id(UUID.randomUUID().toString());
            c1.setCreated_at(new Date());
            c1.setUpdated_at(new Date());
            c1.setPersona(persona.getPersona());
            persona.getPersona().setCuenta(c1);
            persona.guardar();            
            persona.fijarPersona(null);
        }
    }
    
    public Cuenta inicioSesion(String usuario, String clave) {
        return obj.inicioSesion(usuario, clave);
    }
            
}








