/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.servicios;

import controlador.daos.InmuebleDao;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import modelo.Inmueble;

/**
 *
 * @author sissysebas
 */
public class InmuebleService {
    private InmuebleDao obj = new InmuebleDao();
    public Inmueble getInmueble() {
        return obj.getInmueble();
    }
    
    public boolean guardar() {
        return obj.guardar();
    }
    
    public List<Inmueble> todos() {
        return obj.listar();
    }
    
    public Inmueble obtener(Long id) {
        return obj.obtener(id);
    }
    
    public void fijarInmueble(Inmueble inmueble) {
        obj.setInmueble(inmueble);
    }
    
    public List<Inmueble> listarLikeInmueble(String texto) {
        return obj.listarLikeInmueble(texto);
    }
    
    public List<Inmueble> listarInmuebleEstado(boolean estado) {
        return obj.listarInmuebleEstado(estado);
    }
    
    public List<Inmueble> listarInmueblePropietario(String external_propeitrio) {
        return obj.listarInmueblePropietario(external_propeitrio);
    }
            
}

