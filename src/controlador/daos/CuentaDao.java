/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import java.io.File;
import javax.persistence.Query;
import modelo.Cuenta;

/**
 *
 * @author sissysebas
 */
public class CuentaDao extends AdaptadorDao<Cuenta> {
    private Cuenta cuenta;

    public CuentaDao() {
        super(Cuenta.class);        
    }

    public Cuenta getCuenta() {
        if(cuenta == null)
            cuenta = new Cuenta();
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    
    
    
    public boolean guardar() {
        boolean verificar = false;
        try {
            getManager().getTransaction().begin();
            if(cuenta.getId() != null) {
                modificar(cuenta);
            } else {
                guardar(cuenta);
            }
            getManager().getTransaction().commit();
            verificar = true;
        } catch (Exception e) {
            System.out.println("No se ha podido registrar o modificar" + e);
        }
        return verificar;
    }
    
    public Cuenta inicioSesion(String usuario, String clave) {
        Cuenta c =  null;
        try {
            Query q = getManager().createQuery("SELECT c FROM Cuenta c where c.usuario = :user");
            q.setParameter("user", usuario);
            Cuenta aux = (Cuenta)q.getSingleResult();
            if(aux != null && aux.getClave().equals(clave)) {
                c = aux;
            }
            
        } catch (Exception e) {
        }
 
        return c;
    }
    
/**
     * Obtiene el directorio en el cual esta instalado MYSQL
     * @return La direccion path del directorio de MYSQl
     */
   public String baseDirMysql(){
       String query = "SELECT @@basedir";
       String dir="";
       try {
           Query q = this.getManager().createNativeQuery(query);
           for(Object a:q.getResultList()){
               dir = a.toString();
           }
       } catch (Exception e) {
       }
       return dir+File.separatorChar+"bin"+File.separatorChar;
   }    
    
    
    
    
    
    
    
    
}
