/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import controlador.utilidades.Utilidades;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import modelo.Contrato;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class ContratoDao extends AdaptadorDao<Contrato> {
    private Contrato contrato;

    public ContratoDao() {
        super(Contrato.class);        
    }

    public Contrato getContrato() {
        if(contrato == null)
            contrato = new Contrato();
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }
    
    
    
    public boolean guardar() {
        boolean verificar = false;
        try {
            String descripcion = "";
            String accion = "";
            getManager().getTransaction().begin();
            if(contrato.getId() != null) {
                modificar(contrato);
                descripcion = "Se modifica el contrato "+contrato.getId().intValue();
                accion = "Modificar Contrato";
            } else {
                guardar(contrato);
                descripcion = "Se registra un nuevo contrato";
                accion = "Guardar Contrato";
            }
            getManager().getTransaction().commit();
            verificar = true;
            Utilidades.guardarHistorial(accion, descripcion);
        } catch (Exception e) {
            System.out.println("No se ha podido registrar o modificar" + e);
        }
        return verificar;
    }
    
    public List<Contrato> listarLikeContrato(String texto) {
        List<Contrato> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Contrato p where "                    
                    + "(lower(p.persona.apellidos) LIKE CONCAT('%', :texto, '%'))"
                    + " or (lower(p.codigo) LIKE CONCAT('%', :texto1, '%'))");
            q.setParameter("texto", texto);            
            q.setParameter("texto1", texto);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        return lista;
    }
    
    public List<Contrato> listarContratoActivos() {
        List<Contrato> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Contrato p where p.fechaTerminacion IS NULL ORDER BY p.id DESC");            
        //    q.setParameter("fecha", null);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error listarContratoActivos "+e);
        }
        return lista;
    }
    public List<Contrato> listarContratoDesactivos() {
        List<Contrato> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Contrato p where p.fechaTerminacion IS NOT NULL ORDER BY p.id DESC");            
          //  q.setParameter("fecha", null);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error listarContratoDesactivos "+e);
        }
        return lista;
    }
}
