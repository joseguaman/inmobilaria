/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import controlador.utilidades.Utilidades;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import modelo.Inmueble;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class InmuebleDao extends AdaptadorDao<Inmueble> {
    private Inmueble inmueble;

    public InmuebleDao() {
        super(Inmueble.class);        
    }

    public Inmueble getInmueble() {
        if(inmueble == null)
            inmueble = new Inmueble();
        return inmueble;
    }

    public void setInmueble(Inmueble inmueble) {
        this.inmueble = inmueble;
    }
    
    
    
    public boolean guardar() {
        boolean verificar = false;
        try {
            String descripcion = "";
            String accion = "";
            getManager().getTransaction().begin();
            if(inmueble.getId() != null) {
                modificar(inmueble);
                descripcion = "Se modifica el inmueble "+inmueble.getId().intValue();
                accion = "Modificar Inmueble";
            } else {
                guardar(inmueble);
                descripcion = "Se registra un nuevo inmueble";
                accion = "Guardar Inmueble";
            }
            getManager().getTransaction().commit();
            verificar = true;
            Utilidades.guardarHistorial(accion, descripcion);
        } catch (Exception e) {
            System.out.println("No se ha podido registrar o modificar" + e);
        }
        return verificar;
    }
    
    public List<Inmueble> listarLikeInmueble(String texto) {
        List<Inmueble> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Inmueble p where "                    
                    + "(lower(p.persona.apellidos) LIKE CONCAT('%', :texto, '%'))"
                    + " or (lower(p.codigo) LIKE CONCAT('%', :texto1, '%'))");
            q.setParameter("texto", texto);            
            q.setParameter("texto1", texto);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        return lista;
    }
    
    public List<Inmueble> listarInmuebleEstado(boolean estado) {
        List<Inmueble> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Inmueble p where p.estado = :estado");
            q.setParameter("estado", estado);                        
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        return lista;
    }
    
    public List<Inmueble> listarInmueblePropietario(String external_propeitrio) {
        List<Inmueble> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Inmueble p where p.persona.external_id = :external");
            q.setParameter("external", external_propeitrio);                        
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        return lista;
    }
    
}
