/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import modelo.Pago;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class PagoDao extends AdaptadorDao<Pago> {
    private Pago pago;

    public PagoDao() {
        super(Pago.class);        
    }

    public Pago getPago() {
        if(pago == null)
            pago = new Pago();
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }
    
    
    
    public boolean guardar() {
        boolean verificar = false;
        try {
            getManager().getTransaction().begin();
            if(pago.getId() != null) {
                modificar(pago);
            } else {
                guardar(pago);
            }
            getManager().getTransaction().commit();
            verificar = true;
        } catch (Exception e) {
            System.out.println("No se ha podido registrar o modificar" + e);
        }
        return verificar;
    }
    
    public List<Pago> listarPagoEstado(Boolean estado) {
        List<Pago> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Pago p where p.estado = :estado");
            q.setParameter("estado", estado);
            lista = q.getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
    
    public List<Pago> listarPagoClienteEstado(Boolean estado, String external_cliente) {
        List<Pago> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Pago p where p.estado = :estado and p.contrato.externalIdArrendatario = :cliente");
            q.setParameter("estado", estado);
            q.setParameter("cliente", external_cliente);
            lista = q.getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
    
    
    public List<Pago> listarLikePago(String texto) {
        List<Pago> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Pago p where "                    
                    + "(lower(p.persona.apellidos) LIKE CONCAT('%', :texto, '%'))"
                    + " or (lower(p.codigo) LIKE CONCAT('%', :texto1, '%'))");
            q.setParameter("texto", texto);            
            q.setParameter("texto1", texto);
            lista = q.getResultList();
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        return lista;
    }
    //false estan pagados
    //true estan por pagarse
    public List<Pago> listarPagosPagadosInmueble(Boolean estado, String codigo, Date fi, Date ff) {
        List<Pago> lista = new ArrayList<>();
        try {
            Query q = getManager().createQuery("SELECT p FROM Pago p where p.estado = :estado and p.contrato.inmueble.codigo = :codigo and (p.fechaPago >= :fi and p.fechaPago <= :ff)");
            q.setParameter("estado", estado);
            q.setParameter("codigo", codigo);
            q.setParameter("fi", fi);
            q.setParameter("ff", ff);
            lista = q.getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
    
}
