/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.utilidades;

import controlador.daos.CuentaDao;
import controlador.servicios.HistorialService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author sissysebas
 */
public class Utilidades extends StringUtils {

    public static String formatearFecha(Date fecha) {
        String fechaSalida = "";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            fechaSalida = formato.format(fecha);
        } catch (Exception e) {
        }
        return fechaSalida;
    }
    
    public static String formatearFechaSimple(Date fecha) {
        String fechaSalida = "";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaSalida = formato.format(fecha);
        } catch (Exception e) {
        }
        return fechaSalida;
    }

    public static boolean validadorDeCedula(String cedula) {
        boolean cedulaCorrecta = false;

        try {

            if (cedula.length() == 10) // ConstantesApp.LongitudCedula
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
// Coeficientes de validación cédula
// El decimo digito se lo considera dígito verificador
                    int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};
                    int verificador = Integer.parseInt(cedula.substring(9, 10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    } else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            System.out.println("Una excepcion ocurrio en el proceso de validadcion");
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
            System.out.println("La Cédula ingresada es Incorrecta");
        }
        return cedulaCorrecta;
    }
    
    public static void guardarHistorial(String accion, String descripcion) {
        HistorialService hs = new HistorialService();
        hs.getHistorial().setAccion(accion);
        hs.getHistorial().setDecripcion(descripcion);
        hs.getHistorial().setUsuario(Sesion.getCuenta().getPersona().getExternal_id());
        hs.getHistorial().setFechaHora(new Date());
        hs.guardar();
    }
    
    // /** enter
    /**
     * Este método permite obtener la ruta de nuestra aplicación 
     * @return un String La ruta de nuestra aplicación
     */
    public static String getRutaRoot()
    {
        //getProperty de la clase system devuele caracteristicas
        return System.getProperty("user.dir"); //Nos permite obtener la direccion de nuestro sistema 
    }
    /**
     * Retorna la direccion de los contratos guardados
     * @return La ruta de los contactos
     */    
    public static String getRutaContratos() // retorna la dir de la carpeta fotos
        {
        String contratos = Utilidades.getRutaRoot() + File.separatorChar + "contratos";
        File file = new File(contratos);
        if(!file.isDirectory()) {
            file.mkdir();
        }
        return Utilidades.getRutaRoot() + File.separatorChar + "contratos"; //utiliza en separatorChar para no confundirse windows   
    }
    /**
     * Retorna la direccion de los contratos guardados
     * @return LA direccion de los reportes
     */    
    public static String getRutaReportes() // retorna la dir de la carpeta fotos
        {
        String contratos = Utilidades.getRutaRoot() + File.separatorChar + "reportes_inmobiliaria";
        File file = new File(contratos);
        if(!file.isDirectory()) {
            file.mkdir();
        }
        return Utilidades.getRutaRoot() + File.separatorChar + "reportes_inmobiliaria"; //utiliza en separatorChar para no confundirse windows   
    }
    public static void llamarArchivo(String path){
        try {
            Desktop.getDesktop().open(new File(path));
        } catch (Exception e) {
        }
    }
    public static void copiararchivos(String origen, String destino) {
        System.out.println("Desde: " + origen);
        System.out.println("Hacia: " + destino);

        try {
            File inFile = new File(origen);
            File outFile = new File(destino);

            FileInputStream in = new FileInputStream(inFile);
            FileOutputStream out = new FileOutputStream(outFile);

            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }

            in.close();
            out.close();
        } catch (IOException e) {
            System.err.println("Hubo un error de entrada/salida!!!");
        }
    }
    public static double redondear(double num){
        return Math.rint(num*100)/100;
    }
    
    public static Date sumarMesesAnios(String duracion, Date fechaI) {
        String aux [] = duracion.split(" meses");
        Calendar ca = Calendar.getInstance();
        ca.setTime(fechaI);
        ca.add(Calendar.MONTH, Integer.parseInt(aux[0]));
        return ca.getTime();
    }
    
    public static boolean isNumero(String nro) {
        boolean band = false;        
        try {
            Double.parseDouble(nro);
            band = true;
        } catch (Exception e) {
            
        }
        return band;
    }
    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString());
        System.out.println(UUID.randomUUID().toString());
    }
    
    public static String formatearFechaLetras(Date fecha) {
        String fechaSalida = "";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("EEEE MMMM d yyyy");
            fechaSalida = formato.format(fecha);
        } catch (Exception e) {
        }
        return fechaSalida;
    }
    
    public static String backup() {
        String usuario = "root";
        String clave = "";
        String bd = "inmobiliaria";
        String mensaje = "";
        try {//mysqldump -u root -pclave sistemamedico
            String path = Utilidades.getRutaReportes() + File.separatorChar + "backup_.sql";
            JFileChooser fc=new JFileChooser();

//Abrimos la ventana, guardamos la opcion seleccionada por el usuario
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int seleccion=fc.showOpenDialog(null);
        
        if(fc.getSelectedFile()!=null){
            path = fc.getSelectedFile().getPath()+ "backup_.sql";
            String comando = new CuentaDao().baseDirMysql()+"mysqldump -u " + usuario + " -p" + clave + "  " + bd + " > " + path;
            //String comando = new CuentaBD().baseDirMysql() + "mysqldump -u " + PropiedadesSistema.obtenerValor("usuario") + " -p" + PropiedadesSistema.obtenerValor("clave") + " " + PropiedadesSistema.obtenerValor("bd");
            //Process p = Runtime.getRuntime().exec(comando);
            Process runtimeProcess = Runtime.getRuntime().exec(new String[] { "cmd.exe", "/c", comando });
            //System.out.println(executeCmd);
            System.out.println(comando);
            int processComplete = runtimeProcess.waitFor();
            System.out.println("processComplete"+processComplete);
            if (processComplete == 0) {
                mensaje = "Su respaldo de base de datos en: " + path;

            } else {
                mensaje = "No se pudo crear el respaldo";
            }
//            InputStream is = p.getInputStream();
//            FileOutputStream fos = new FileOutputStream(PropiedadesSistema.obtenerValor("reportes") + File.separatorChar + "backup_.sql");
//            byte[] buffer = new byte[1000];//los bytes me guardan los bytes producto de la ejecucion del programa msqldum
//
//            int leido = is.read(buffer);
//            while (leido > 0) {
//                fos.write(buffer, 0, leido);
//                leido = is.read(buffer);
//            }
//
//            fos.close();
            //System.out.println(comando);
            mensaje = "Su respaldo de base de datos en: " + Utilidades.getRutaReportes() + File.separatorChar + "backup_.sql";
        }else{
            JOptionPane.showMessageDialog(null, "Escoja un directorio");
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(mensaje);
        return mensaje;
    }
    
}
