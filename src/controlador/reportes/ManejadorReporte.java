/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.reportes;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import controlador.servicios.ContratoService;
import controlador.servicios.PagoService;
import controlador.utilidades.Utilidades;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import modelo.Contrato;
import modelo.Inmueble;
import modelo.Persona;

import org.apache.commons.lang.SystemUtils;
import vista.tablas.DataPago;

/**
 *
 * @author sissysebas
 */
public class ManejadorReporte {
    public static void crearReportePagos(DataPago data) {
        try {
            Document documento = new Document(PageSize.A4_LANDSCAPE);
            String fileName = Utilidades.getRutaReportes() + SystemUtils.FILE_SEPARATOR + "recibo.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);
            writer.setInitialLeading(1);
            documento.open();
            
            
            documento.add(UtilidadesReportes.parrafo("RECIBO DE PAGO", Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            documento.add(UtilidadesReportes.parrafo("Cliente: " + data.cliente, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));            
            documento.add(UtilidadesReportes.parrafo("Fecha: " + data.fechaPago, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            //documento.add(UtilidadesReportes.parrafo("Se ha cancelado el pago correspondiente a la fecha " + data.fechaLetras + " la cantidad de " + String.valueOf(data.pago) + " por concepto de pago de arriendo al contrato nro  " + data.nroContrato + " del inmueble " + data.propiedad, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            PdfPTable tablam = new PdfPTable(4);
            tablam.addCell(UtilidadesReportes.celda("Cant", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Descripcion", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("P/U", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("P/T", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            
            
                //String datos[] = a.split(";");
                //dataset.setValue(datos[0], new Double(datos[2]));
                tablam.addCell(UtilidadesReportes.celda("1", Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda("Se ha cancelado el pago correspondiente a la fecha " + data.fechaLetras + " la cantidad de " + String.valueOf(data.pago) + " por concepto de pago de arriendo al contrato nro  " + data.nroContrato + " del inmueble " + data.propiedad, Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(data.pago), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(data.pago) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                //tablam.addCell(UtilidadesReportes.celda(datos[2], Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda("Subtotal:", Font.NORMAL, 8, "arial", Element.ALIGN_RIGHT, true, 3));
            tablam.addCell(UtilidadesReportes.celda(String.valueOf(data.pago), Font.NORMAL, 8, "arial", Element.ALIGN_JUSTIFIED, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Subtotal:", Font.NORMAL, 8, "arial", Element.ALIGN_RIGHT, true, 3));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(data.pago) , Font.NORMAL, 8, "arial", Element.ALIGN_JUSTIFIED, true, 0));
            documento.add(tablam);
            
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            documento.add(UtilidadesReportes.parrafo(data.cliente, Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            //documento.add(tablam);
            documento.close();
            Utilidades.llamarArchivo(fileName);
            
        } catch (Exception e) {
        }
    }
    
    public static void reportePropietarios(List<Persona> lista) {
        try {
            Document documento = new Document(PageSize.A4_LANDSCAPE);
            String fileName = Utilidades.getRutaReportes() + SystemUtils.FILE_SEPARATOR + "recibo.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);
            writer.setInitialLeading(1);
            documento.open();
            
            
            documento.add(UtilidadesReportes.parrafo("LISTADO DE PROPIETARIOS", Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            documento.add(UtilidadesReportes.parrafo("Fecha: " + Utilidades.formatearFechaSimple(new Date()), Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            PdfPTable tablam = new PdfPTable(4);
            tablam.addCell(UtilidadesReportes.celda("Cedula", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Propietario", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Telefono", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Nro propiedades", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            
            for (Persona p : lista) {
                //String datos[] = a.split(";");
                //dataset.setValue(datos[0], new Double(datos[2]));
                tablam.addCell(UtilidadesReportes.celda(p.getCedula(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.toString(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.getTelefono(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(p.getListaInmuebles().size()) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                //tablam.addCell(UtilidadesReportes.celda(datos[2], Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
            }
            documento.add(tablam);
            
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            
            //documento.add(tablam);
            documento.close();
            Utilidades.llamarArchivo(fileName);
            
        } catch (Exception e) {
        }
    }
    
    public static void reporteCliente(List<Persona> lista) {
        try {
            Document documento = new Document(PageSize.A4_LANDSCAPE);
            String fileName = Utilidades.getRutaReportes() + SystemUtils.FILE_SEPARATOR + "recibo.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);
            writer.setInitialLeading(1);
            documento.open();
            
            
            documento.add(UtilidadesReportes.parrafo("LISTADO DE ARRENDATARIOS", Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            documento.add(UtilidadesReportes.parrafo("Fecha: " + Utilidades.formatearFechaSimple(new Date()), Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            PdfPTable tablam = new PdfPTable(4);
            tablam.addCell(UtilidadesReportes.celda("Cedula", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Propietario", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Telefono", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));            
            tablam.addCell(UtilidadesReportes.celda("Nro pagos pendientes", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            
            for (Persona p : lista) {
                //String datos[] = a.split(";");
                //dataset.setValue(datos[0], new Double(datos[2]));
                tablam.addCell(UtilidadesReportes.celda(p.getCedula(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.toString(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.getTelefono(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(new PagoService().listarPagoClienteEstado(Boolean.TRUE, p.getExternal_id()).size()) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                
                //tablam.addCell(UtilidadesReportes.celda(datos[2], Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
            }
            documento.add(tablam);
            
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            
            //documento.add(tablam);
            documento.close();
            Utilidades.llamarArchivo(fileName);
            
        } catch (Exception e) {
        }
    }
    
    public static void reporteInmueble(List<Inmueble> lista) {
        try {
            Document documento = new Document(PageSize.A4_LANDSCAPE);
            String fileName = Utilidades.getRutaReportes() + SystemUtils.FILE_SEPARATOR + "recibo.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);
            writer.setInitialLeading(1);
            documento.open();
            
            
            documento.add(UtilidadesReportes.parrafo("LISTADO DE ARRENDATARIOS", Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            documento.add(UtilidadesReportes.parrafo("Fecha: " + Utilidades.formatearFechaSimple(new Date()), Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            PdfPTable tablam = new PdfPTable(7);
            tablam.addCell(UtilidadesReportes.celda("Propietario", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Tipo", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            
            tablam.addCell(UtilidadesReportes.celda("Prestado", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));            
            tablam.addCell(UtilidadesReportes.celda("Nro pisos", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Nro habitaciones", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Direccion", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Precio", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            
            for (Inmueble p : lista) {
                //String datos[] = a.split(";");
                //dataset.setValue(datos[0], new Double(datos[2]));
                tablam.addCell(UtilidadesReportes.celda(p.getPersona().toString(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.getTipo(), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda((p.getEstado() ? "SI" : "NO"), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(p.getNroPisos()) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(p.getNroHabitaciones()) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(p.getCiudad()+" "+p.getBarrio()+" "+p.getCallePrincipal()+" "+p.getCalleSecundaria() , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(p.getPrecio()) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                
                //tablam.addCell(UtilidadesReportes.celda(datos[2], Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
            }
            documento.add(tablam);
            
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            
            //documento.add(tablam);
            documento.close();
            Utilidades.llamarArchivo(fileName);
            
        } catch (Exception e) {
        }
    }
    
    public static void crearReportePagosInmueble(List<DataPago> data, String propietario, String fi, String ff) {
        try {
            Document documento = new Document(PageSize.A4_LANDSCAPE);
            String fileName = Utilidades.getRutaReportes() + SystemUtils.FILE_SEPARATOR + "recibo.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream(fileName);
            PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);
            writer.setInitialLeading(1);
            documento.open();
            
            
            documento.add(UtilidadesReportes.parrafo("LISTADO DE PAGOS", Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            documento.add(UtilidadesReportes.parrafo("Propietario: " + propietario, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));            
            documento.add(UtilidadesReportes.parrafo("Fecha de Inicio: " + fi, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("Fecha de finalizacion: " + ff, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            //documento.add(UtilidadesReportes.parrafo("Nro Contrato: " + data.nroContrato, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            //documento.add(UtilidadesReportes.parrafo("Se ha cancelado el pago correspondiente a la fecha " + data.fechaLetras + " la cantidad de " + String.valueOf(data.pago) + " por concepto de pago de arriendo al contrato nro  " + data.nroContrato + " del inmueble " + data.propiedad, Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            PdfPTable tablam = new PdfPTable(4);
            tablam.addCell(UtilidadesReportes.celda("Cant", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Descripcion", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("P/U", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            tablam.addCell(UtilidadesReportes.celda("P/T", Font.BOLD, 8, "arial", Element.ALIGN_CENTER, true, 0));
            int i = 1;
            double subtotal = 0.0;
            for(DataPago pagos : data) {
                //String datos[] = a.split(";");
                //dataset.setValue(datos[0], new Double(datos[2]));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(i), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda("Se ha cancelado el pago correspondiente a la fecha " + pagos.fechaLetras + " la cantidad de " + String.valueOf(pagos.pago) + " por concepto de pago de arriendo al contrato nro  " + pagos.nroContrato + " del inmueble " + pagos.propiedad, Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(pagos.pago), Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(pagos.pago) , Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                //tablam.addCell(UtilidadesReportes.celda(datos[2], Font.NORMAL, 8, "arial", Element.ALIGN_CENTER, true, 0));
                subtotal += pagos.pago;
                i++;
            }
            tablam.addCell(UtilidadesReportes.celda("Subtotal:", Font.NORMAL, 8, "arial", Element.ALIGN_RIGHT, true, 3));
            tablam.addCell(UtilidadesReportes.celda(String.valueOf(subtotal), Font.NORMAL, 8, "arial", Element.ALIGN_JUSTIFIED, true, 0));
            tablam.addCell(UtilidadesReportes.celda("Total:", Font.NORMAL, 8, "arial", Element.ALIGN_RIGHT, true, 3));
                tablam.addCell(UtilidadesReportes.celda(String.valueOf(subtotal) , Font.NORMAL, 8, "arial", Element.ALIGN_JUSTIFIED, true, 0));
            documento.add(tablam);
            
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            documento.add(UtilidadesReportes.parrafo("   ", Font.BOLD, 9, "arial", Element.ALIGN_JUSTIFIED));
            
            //documento.add(UtilidadesReportes.parrafo(data.cliente, Font.BOLD, 9, "arial", Element.ALIGN_CENTER));
            //documento.add(tablam);
            documento.close();
            Utilidades.llamarArchivo(fileName);
            
        } catch (Exception e) {
        }
    }
    
}
