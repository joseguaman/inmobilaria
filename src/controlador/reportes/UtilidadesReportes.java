/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.reportes;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import java.awt.Color;

/**
 *
 * @author janeth
 */
public class UtilidadesReportes {
    /**
     * Permite crear un parrafo
     * @param texto texto a llevar
     * @param estilo Si es negrita, cursiva, etc
     * @param letra, tamano de letra
     * @param family tipo de letra
     * @param aling Alinecion
     * @return Paragraph
     */
    public static Paragraph parrafo(String texto, int estilo, int letra, String family, int aling) {
        Paragraph titulo = new Paragraph(texto, FontFactory.getFont(family, // fuente
                letra, // tamaño
                estilo, // estilo
                BaseColor.BLACK));
        titulo.setAlignment(aling);
        return titulo;
    }
    /**
     * Permite crear celdas con espacios
     * @param texto texto a llevar
     * @param estilo Si es negrita, cursiva, etc
     * @param letra, tamano de letra
     * @param family tipo de letra
     * @param aling Alinecion
     * @param border si es false se quita el border
     * @param colSpan cuantas columnas debe colapsar
     * @return PdfPCell
     */
    public static PdfPCell celdaSpacing(String texto, int estilo, int letra, String family, int aling, boolean border, int colSpan) {
        PdfPCell celda = new PdfPCell();
        celda.addElement(UtilidadesReportes.parrafo(texto, estilo, letra, family, aling));
        celda.setPadding(8);
        if(!border)
            celda.setBorder(0);
        if(colSpan >= 2)
            celda.setColspan(colSpan);
        return celda;
    }
    /**
     * Permite crear celdas sin espacios
     * @param texto texto a llevar
     * @param estilo Si es negrita, cursiva, etc
     * @param letra, tamano de letra
     * @param family tipo de letra
     * @param aling Alinecion
     * @param border si es false se quita el border
     * @param colSpan cuantas columnas debe colapsar
     * @return PdfPCell
     */
    public static PdfPCell celda(String texto, int estilo, int letra, String family, int aling, boolean border, int colSpan) {
        PdfPCell celda = new PdfPCell();
        celda.addElement(UtilidadesReportes.parrafo(texto, estilo, letra, family, aling));
        celda.setPadding(5);
        if(!border)
            celda.setBorder(0);
        if(colSpan >= 2)
            celda.setColspan(colSpan);
        return celda;
    }
    /**
     * Permite crear celdas con espacios
     * @param texto texto a llevar
     * @param estilo Si es negrita, cursiva, etc
     * @param letra, tamano de letra
     * @param family tipo de letra
     * @param aling Alinecion
     * @param border si es false se quita el border
     * @param colSpan cuantas columnas debe colapsar     
     * @param color Color de la celda
     * @return PdfPCell
     */
    public static PdfPCell celdaColor(String texto, int estilo, int letra, String family, int aling, boolean border, int colSpan, BaseColor color) {
        PdfPCell celda = new PdfPCell();
        celda.addElement(UtilidadesReportes.parrafo(texto, estilo, letra, family, aling));
        celda.setPadding(5);
        celda.setBackgroundColor(color);
        if(!border)
            celda.setBorder(0);
        if(colSpan >= 2)
            celda.setColspan(colSpan);
        return celda;
    }
    
}
