package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Usuario
 */
@Getter
@Setter
@Entity
public class Contrato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 20)
    private String numeroContrato;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinalizacion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaTerminacion;
    @Column(length = 21)
    private String duracion;
    private String observaciones;
    private String archivo;
    @Column(length = 40)
    private String externalIdArrendatario;
    @Column(length = 40)
    private String external_id;
    private Double garantia;
    private Double precio;    
    @ManyToOne (cascade = CascadeType.REFRESH)
    @JoinColumn (referencedColumnName = "id", name = "idInmueble")
    private Inmueble inmueble;
    @OneToMany (cascade = CascadeType.ALL, mappedBy = "contrato")
    private List<Pago> listaPagos= new ArrayList<>();
    @OneToOne (cascade = CascadeType.ALL, mappedBy = "contrato")
    private Finalizacion finalizacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Contrato[ id=" + id + " ]";
    }

}
