/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.servicios.PersonaService;
import controlador.servicios.RolService;
import controlador.utilidades.Utilidades;
import java.util.UUID;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import vista.tablas.NodeloTablaPersona;
import vista.utilidades.UtilidadesConponente;
import vista.utilidades.validadores.EventoEntero;
import vista.utilidades.validadores.EventoTecladoLetras;
import vista.utilidades.validadores.EventoTecladoLetrasNumeros;

/**
 *
 * @author sissysebas
 */
public class FrmCliente extends javax.swing.JDialog {

    private PersonaService ps = new PersonaService();
    private NodeloTablaPersona modelo = new NodeloTablaPersona();
    private FrmContrato frmcontrato;

    /**
     * Creates new form FrmCliente
     */
    public FrmCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttonGroup1.add(rb_cl);
        buttonGroup1.add(rb_pr);
        ButtonGroup g2 = new ButtonGroup();
        g2.add(rd_t);
        g2.add(rd_p);
        g2.add(rd_c);
        txt_cedula.addKeyListener(new EventoEntero(txt_cedula, 10));
        txt_nombre.addKeyListener(new EventoTecladoLetras(txt_nombre));
        txt_apellidos.addKeyListener(new EventoTecladoLetras(txt_apellidos));
        txt_dir.addKeyListener(new EventoTecladoLetrasNumeros(txt_nombre));
        txt_fono.addKeyListener(new EventoEntero(txt_fono, 10));
        limpiar();
    }

    public FrmCliente(java.awt.Frame parent, boolean modal, FrmContrato frm) {
        super(parent, modal);
        initComponents();
        buttonGroup1.add(rb_cl);
        buttonGroup1.add(rb_pr);
        ButtonGroup g2 = new ButtonGroup();
        g2.add(rd_t);
        g2.add(rd_p);
        g2.add(rd_c);
        limpiar();
        //btn_guardar.setEnabled(false);
        btn_cancelar.setEnabled(false);
        //btn_nuevo.setEnabled(false);
        btn_editar.setText("Escoger");
        frmcontrato = frm;
    }

    private void cargarTabla() {
        modelo.setLista(ps.listarSinAdministrador());
        tbl_tabla.setModel(modelo);
        tbl_tabla.updateUI();
    }

    private void buscar() {
        if (rd_t.isSelected()) {
            cargarTabla();

        } else {
            String tipo = (rd_p.isSelected()) ? "Propietario" : "Arrendatario";
            modelo.setLista(ps.listarSinAdministradorTipo(tipo));
            tbl_tabla.setModel(modelo);
            tbl_tabla.updateUI();
        }

    }

    private void buscarTexto() {
        if (txt_buscar.getText().trim().length() >= 3) {
            if (rd_t.isSelected()) {
                modelo.setLista(ps.listarSinAdministradorLike(txt_buscar.getText()));
            } else {
                String tipo = (rd_p.isSelected()) ? "Propietario" : "Arrendatario";
                modelo.setLista(ps.listarSinAdministradorTipoLike(tipo, txt_buscar.getText()));
            }
            tbl_tabla.setModel(modelo);
            tbl_tabla.updateUI();
        } else {
            buscar();
        }
    }

    private void limpiar() {
        txt_apellidos.setText("");
        txt_buscar.setText("");
        txt_cedula.setText("");
        txt_dir.setText("");
        txt_fono.setText("");
        txt_nombre.setText("");
        cargarTabla();
        rb_cl.setSelected(false);
        rb_pr.setSelected(false);
        rd_t.setSelected(true);
        rd_p.setSelected(false);
        txt_cedula.setEditable(true);
        ps.fijarPersona(null);
    }

    private void cargarObjeto() {
        ps.getPersona().setApellidos(txt_apellidos.getText());
        ps.getPersona().setNombres(txt_nombre.getText());
        ps.getPersona().setCedula(txt_cedula.getText());
        ps.getPersona().setDireccion(txt_dir.getText());
        ps.getPersona().setTelefono(txt_fono.getText());
        if (rb_pr.isSelected()) {
            ps.getPersona().setRol(new RolService().buscarRolNombre("Propietario"));
        } else {
            ps.getPersona().setRol(new RolService().buscarRolNombre("Arrendatario"));
        }
    }

    private void guardar() {
        String mensaje = "Se requiere este dato";
        if (!UtilidadesConponente.mostrarError(txt_cedula, mensaje, 'r')
                && !UtilidadesConponente.mostrarError(txt_apellidos, mensaje, 'r')
                && !UtilidadesConponente.mostrarError(txt_nombre, mensaje, 'r')
                && !UtilidadesConponente.mostrarError(txt_dir, mensaje, 'r')
                && !UtilidadesConponente.mostrarError(txt_fono, mensaje, 'r')) {
            cargarObjeto();
            if (ps.getPersona().getId() != null) {
                //modificar
                if (ps.guardar()) {
                    UtilidadesConponente.mensajeOk("OK", "Se ha modificado correctamente");
                    limpiar();
                } else {
                    UtilidadesConponente.mensajeError("Error", "No se pudo modificar");
                }
            } else {
                //guardar
                if (Utilidades.validadorDeCedula(txt_cedula.getText())) {
                    if (ps.getPersonaCedula(txt_cedula.getText()) != null) {
                        UtilidadesConponente.mensajeError("Error de cedula", "Cedula ya registrada");
                    } else {
                        //guardar
                        ps.getPersona().setExternal_id(UUID.randomUUID().toString());
                        if (ps.guardar()) {
                            UtilidadesConponente.mensajeOk("OK", "Se ha registrado correctamente");
                            limpiar();
                        } else {
                            UtilidadesConponente.mensajeError("Error", "No se pudo guardar");
                        }
                    }
                } else {
                    UtilidadesConponente.mensajeError("Error de cedula", "Cedula no valida");
                }
            }
        }
    }
    
    private void activar_desactivar() {
        int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            PersonaService p = new PersonaService();
            p.fijarPersona(modelo.getLista().get(fila));
            if(p.getPersona().isEstado()) {
                int i = JOptionPane.showConfirmDialog(this, "Desea desactivar esta persona?");
                if(i == JOptionPane.OK_OPTION) {
                    p.getPersona().setEstado(false);
                    p.guardar();
                    UtilidadesConponente.mensajeOk("OK", "Se ha desactivado correctamente");
                    limpiar();
                }
            } else {
                int i = JOptionPane.showConfirmDialog(this, "Desea activar esta persona?");
                if(i == JOptionPane.OK_OPTION) {
                    p.getPersona().setEstado(true);
                    p.guardar();
                    UtilidadesConponente.mensajeOk("OK", "Se ha activado correctamente");
                    limpiar();
                }
            }
        } else {
            UtilidadesConponente.mensajeError("Error", "Escoja un dato de la tabla");
        }
    }

    private void cargarVista() {
        int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            ps.fijarPersona(modelo.getLista().get(fila));
            if (frmcontrato != null) {
                if(modelo.getLista().get(fila).isEstado()) {
                    frmcontrato.getPersonaService().fijarPersona(modelo.getLista().get(fila));
                    this.dispose();
                } else {
                    UtilidadesConponente.mensajeError("Error", "Escoja un cliente activo");
                }
            } else {
                txt_apellidos.setText(ps.getPersona().getApellidos());
                txt_nombre.setText(ps.getPersona().getNombres());
                txt_cedula.setText(ps.getPersona().getCedula());
                txt_cedula.setEditable(false);
                txt_dir.setText(ps.getPersona().getDireccion());
                txt_fono.setText(ps.getPersona().getTelefono());
                if (ps.getPersona().getRol().getNombre().equalsIgnoreCase("Propietario")) {
                    rb_pr.setSelected(true);
                } else {
                    rb_cl.setSelected(true);
                }
            }
        } else {
            UtilidadesConponente.mensajeError("Error", "Escoja un dato de la tabla");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        panelReflect1 = new org.edisoncor.gui.panel.PanelReflect();
        btn_guardar = new org.edisoncor.gui.button.ButtonAeroLeft();
        jLabel2 = new javax.swing.JLabel();
        txt_cedula = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_fono = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel3 = new javax.swing.JLabel();
        btn_cancelar = new org.edisoncor.gui.button.ButtonAeroRight();
        jLabel6 = new javax.swing.JLabel();
        rb_cl = new javax.swing.JRadioButton();
        rb_pr = new javax.swing.JRadioButton();
        labelCustom1 = new org.edisoncor.gui.label.LabelCustom();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_dir = new javax.swing.JTextArea();
        txt_nombre = new org.edisoncor.gui.textField.TextFieldRound();
        txt_apellidos = new org.edisoncor.gui.textField.TextFieldRound();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_buscar = new org.edisoncor.gui.textField.TextFieldRound();
        btn_buscar = new org.edisoncor.gui.button.ButtonAeroRight();
        rd_p = new javax.swing.JRadioButton();
        rd_t = new javax.swing.JRadioButton();
        rd_c = new javax.swing.JRadioButton();
        labelCustom2 = new org.edisoncor.gui.label.LabelCustom();
        btn_editar = new org.edisoncor.gui.button.ButtonAeroLeft();
        btn_nuevo = new org.edisoncor.gui.button.ButtonAeroRight();
        btn_salir = new org.edisoncor.gui.button.ButtonCircle();
        buttonRect1 = new org.edisoncor.gui.button.ButtonRect();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(null);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(20, 150, 610, 150);

        panelReflect1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect1.setLayout(null);

        btn_guardar.setBackground(new java.awt.Color(31, 157, 76));
        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        panelReflect1.add(btn_guardar);
        btn_guardar.setBounds(200, 230, 110, 29);

        jLabel2.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel2.setText("Cedula:");
        panelReflect1.add(jLabel2);
        jLabel2.setBounds(30, 80, 80, 16);
        panelReflect1.add(txt_cedula);
        txt_cedula.setBounds(110, 70, 180, 30);

        jLabel1.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel1.setText("Direccion:");
        panelReflect1.add(jLabel1);
        jLabel1.setBounds(20, 150, 90, 16);

        jLabel4.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel4.setText("Telefono:");
        panelReflect1.add(jLabel4);
        jLabel4.setBounds(330, 80, 80, 16);
        panelReflect1.add(txt_fono);
        txt_fono.setBounds(410, 70, 180, 30);

        jLabel3.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel3.setText("Nombres:");
        panelReflect1.add(jLabel3);
        jLabel3.setBounds(330, 110, 80, 16);

        btn_cancelar.setBackground(new java.awt.Color(229, 14, 14));
        btn_cancelar.setText("Cancelar");
        panelReflect1.add(btn_cancelar);
        btn_cancelar.setBounds(320, 230, 120, 29);

        jLabel6.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel6.setText("Apellidos:");
        panelReflect1.add(jLabel6);
        jLabel6.setBounds(30, 110, 80, 16);

        rb_cl.setSelected(true);
        rb_cl.setText("Cliente");
        panelReflect1.add(rb_cl);
        rb_cl.setBounds(330, 200, 150, 23);

        rb_pr.setText("Propietario");
        panelReflect1.add(rb_pr);
        rb_pr.setBounds(110, 200, 210, 23);

        labelCustom1.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom1.setText("DATOS DE PERSONA");
        panelReflect1.add(labelCustom1);
        labelCustom1.setBounds(10, 10, 600, 50);

        jLabel7.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel7.setText("Tipo:");
        panelReflect1.add(jLabel7);
        jLabel7.setBounds(30, 200, 80, 16);

        txt_dir.setColumns(20);
        txt_dir.setRows(5);
        jScrollPane2.setViewportView(txt_dir);

        panelReflect1.add(jScrollPane2);
        jScrollPane2.setBounds(120, 150, 460, 40);
        panelReflect1.add(txt_nombre);
        txt_nombre.setBounds(410, 110, 180, 30);
        panelReflect1.add(txt_apellidos);
        txt_apellidos.setBounds(110, 110, 180, 30);

        jPanel2.add(panelReflect1);
        panelReflect1.setBounds(20, 360, 620, 270);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel5.setText("Buscar:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(20, 20, 80, 16);

        txt_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_buscarActionPerformed(evt);
            }
        });
        jPanel1.add(txt_buscar);
        txt_buscar.setBounds(90, 10, 120, 30);

        btn_buscar.setBackground(new java.awt.Color(14, 62, 229));
        btn_buscar.setText("BUSCAR");
        jPanel1.add(btn_buscar);
        btn_buscar.setBounds(530, 10, 80, 29);

        rd_p.setText("Propietario");
        rd_p.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rd_pItemStateChanged(evt);
            }
        });
        jPanel1.add(rd_p);
        rd_p.setBounds(220, 10, 98, 23);

        rd_t.setText("Todos");
        rd_t.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rd_tItemStateChanged(evt);
            }
        });
        jPanel1.add(rd_t);
        rd_t.setBounds(420, 10, 68, 23);

        rd_c.setText("Cliente");
        rd_c.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rd_cItemStateChanged(evt);
            }
        });
        jPanel1.add(rd_c);
        rd_c.setBounds(330, 10, 73, 23);

        jPanel2.add(jPanel1);
        jPanel1.setBounds(20, 80, 620, 50);

        labelCustom2.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom2.setText("ADMINISTRAR PERSONAS");
        jPanel2.add(labelCustom2);
        labelCustom2.setBounds(10, 10, 630, 50);

        btn_editar.setBackground(new java.awt.Color(31, 157, 76));
        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_editar);
        btn_editar.setBounds(140, 320, 110, 29);

        btn_nuevo.setBackground(new java.awt.Color(229, 14, 14));
        btn_nuevo.setText("Nuevo");
        btn_nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nuevoActionPerformed(evt);
            }
        });
        jPanel2.add(btn_nuevo);
        btn_nuevo.setBounds(370, 320, 120, 29);

        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });
        jPanel2.add(btn_salir);
        btn_salir.setBounds(580, 310, 46, 40);

        buttonRect1.setText("Act/Desc");
        buttonRect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRect1ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonRect1);
        buttonRect1.setBounds(260, 320, 100, 29);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 650, 640);

        setSize(new java.awt.Dimension(659, 674));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void btn_nuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nuevoActionPerformed
        // TODO add your handling code here:
        limpiar();
    }//GEN-LAST:event_btn_nuevoActionPerformed

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        // TODO add your handling code here:
        cargarVista();
    }//GEN-LAST:event_btn_editarActionPerformed

    private void rd_pItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rd_pItemStateChanged
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_rd_pItemStateChanged

    private void rd_cItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rd_cItemStateChanged
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_rd_cItemStateChanged

    private void rd_tItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rd_tItemStateChanged
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_rd_tItemStateChanged

    private void txt_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_buscarActionPerformed
        // TODO add your handling code here:
        buscarTexto();
    }//GEN-LAST:event_txt_buscarActionPerformed

    private void buttonRect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRect1ActionPerformed
        // TODO add your handling code here:
        activar_desactivar();
    }//GEN-LAST:event_buttonRect1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmCliente dialog = new FrmCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonAeroRight btn_buscar;
    private org.edisoncor.gui.button.ButtonAeroRight btn_cancelar;
    private org.edisoncor.gui.button.ButtonAeroLeft btn_editar;
    private org.edisoncor.gui.button.ButtonAeroLeft btn_guardar;
    private org.edisoncor.gui.button.ButtonAeroRight btn_nuevo;
    private org.edisoncor.gui.button.ButtonCircle btn_salir;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.edisoncor.gui.button.ButtonRect buttonRect1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private org.edisoncor.gui.label.LabelCustom labelCustom1;
    private org.edisoncor.gui.label.LabelCustom labelCustom2;
    private org.edisoncor.gui.panel.PanelReflect panelReflect1;
    private javax.swing.JRadioButton rb_cl;
    private javax.swing.JRadioButton rb_pr;
    private javax.swing.JRadioButton rd_c;
    private javax.swing.JRadioButton rd_p;
    private javax.swing.JRadioButton rd_t;
    private javax.swing.JTable tbl_tabla;
    private org.edisoncor.gui.textField.TextFieldRound txt_apellidos;
    private org.edisoncor.gui.textField.TextFieldRound txt_buscar;
    private org.edisoncor.gui.textField.TextFieldRound txt_cedula;
    private javax.swing.JTextArea txt_dir;
    private org.edisoncor.gui.textField.TextFieldRound txt_fono;
    private org.edisoncor.gui.textField.TextFieldRound txt_nombre;
    // End of variables declaration//GEN-END:variables
}
