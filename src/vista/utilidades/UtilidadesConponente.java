/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.utilidades;


import com.toedter.calendar.JDateChooser;
import controlador.servicios.PagoService;
import controlador.servicios.PersonaService;
import controlador.utilidades.Utilidades;
import java.awt.Color;
import java.awt.Component;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.JTextComponent;
import modelo.Contrato;
import modelo.Finalizacion;
import modelo.Inmueble;
import modelo.Pago;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class UtilidadesConponente {

    public static boolean mostrarError(JComponent componente, String mensaje, char tipo) {
        boolean band = true;
        switch (tipo) {
            case 'r':
                if (componente instanceof JTextComponent) {
                    JTextComponent txt = (JTextComponent) componente;
                    if (Utilidades.isEmpty(txt.getText())) {
                        componente.setBackground(Color.red);
                        componente.setToolTipText(mensaje);
                    } else {
                        componente.setBackground(Color.white);
                        componente.setToolTipText(null);
                        band = false;
                    }
                } else if (componente instanceof JDateChooser) {
                    JDateChooser txt = (JDateChooser) componente;
                    if (txt.getDate() != null && Utilidades.isEmpty(txt.getDate().toString())) {
                        componente.setBackground(Color.red);
                        componente.setToolTipText(mensaje);
                    } else {
                        componente.setBackground(Color.white);
                        componente.setToolTipText(null);
                        band = false;
                    }
                }
                break;
            case 'n' :
                
                if (componente instanceof JTextComponent) {
                    JTextComponent txt = (JTextComponent) componente;
                    System.out.println(txt.getText());
                    if (!Utilidades.isNumero(txt.getText())) {
                        componente.setBackground(Color.red);
                        componente.setToolTipText(mensaje);
                    } else {
                        componente.setBackground(Color.white);
                        componente.setToolTipText(null);
                        band = false;
                    }
                }
                break;
            
        }

        return band;
    }

    public static void mensajeError(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    public static void mensajeOk(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void llenarComboPropietarios(PersonaService ps, JComboBox<Persona> cbx) {
        cbx.removeAllItems();
        for(Persona p : ps.listarSinAdministradorTipo("Propietario")) {
            if(p.isEstado())
                cbx.addItem(p);
        }
    }
    
    public static Persona obtenerPersonaCombo(JComboBox<Persona> cbx) {
        return (Persona)cbx.getSelectedItem();
    }
    
    public static void llenarComboInmueblesLibres(JComboBox cbx, List<Inmueble> listado) {
        cbx.removeAllItems();
        for(Inmueble i : listado) {
            cbx.addItem(i);
        }
    }
    
    public static Inmueble obtenerInmueble(JComboBox<Inmueble> cbx) {
        return (Inmueble)cbx.getSelectedItem();
    }
    
    public static Contrato crearCuotas(Contrato contrato) {
        String aux [] = contrato.getDuracion().split(" meses");
        int meses = Integer.parseInt(aux[0]);
        
        for(int i = 0; i < meses; i++) {
            int a = i+1;
            Calendar calendario = Calendar.getInstance();
            calendario.setTime(contrato.getFechaInicio());
            calendario.add(Calendar.MONTH, a);
            Pago pago = new Pago();
            //pago.setFechaPago(calendario.getTime());
            pago.setContrato(contrato);
            pago.setEstado(Boolean.TRUE);
            pago.setMesPago(Utilidades.formatearFechaLetras(calendario.getTime()));
            pago.setValor(contrato.getPrecio());
            contrato.getListaPagos().add(pago);
        }
        return contrato;
    }
    
    public static void cancelarPagosMasivo(Contrato contrato) {
        for(Pago pago : contrato.getListaPagos()) {
            PagoService pagoService = new PagoService();
            pagoService.fijarPago(pago);
            pagoService.getPago().setEstado(Boolean.FALSE);
            pagoService.guardar();
        }
    }
    
    public static Finalizacion finalizacion(Contrato contrato, Double devolucion, String motivo, String observacion) {
        Finalizacion finalizacion = new Finalizacion();
        finalizacion.setContrato(contrato);
        finalizacion.setDevolucion(devolucion);
        finalizacion.setMotivo(motivo);
        finalizacion.setObservaciones(observacion);
        finalizacion.setFecha(new Date());
        return finalizacion;
    }
    
    public static void tablePack(JTable table) {
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (int i = 0; i < table.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 0;

            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < table.getRowCount(); r++) {
                renderer = table.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(table, table.getValueAt(r, i),
                        false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            
            col.setPreferredWidth(width + 2);
        }
    }
    
    public static void cargarComboInmueblePr(JComboBox cbx, Persona p) {
        cbx.removeAllItems();
        for(Inmueble i: p.getListaInmuebles()) {
            if(i.getActivo_desactivo())
                cbx.addItem(i);
        }
    }
    
    public static boolean puedePagar(Contrato c, Pago pa) {
        boolean badn = false;
        List<Pago> lista = c.getListaPagos().stream()
                                .filter(t -> {
                                    if(t.getEstado() )
                                        return true;
                                     else 
                                        return false;
                                })
                                .collect(Collectors.toList());
        lista.sort((p1, p2) -> p1.getId().compareTo(p2.getId()));
        int cont = 0;
        for(Pago p : lista) {
            System.out.println("xxxxx "+ p.getId().intValue() + "  "+pa.getId().intValue());
            if(p.getId().intValue() == pa.getId().intValue()) {                
                break;
            }
            cont++;
        }
        if(cont == 0) {
            badn = true;
        }
        return badn;
    }
    
}