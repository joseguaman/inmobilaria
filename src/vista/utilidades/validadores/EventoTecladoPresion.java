package vista.utilidades.validadores;

import java.awt.event.KeyAdapter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author janeth
 */
public class EventoTecladoPresion extends KeyAdapter {

    private JTextComponent componente;
    private int min = 0;
    private int max = 0;
    private boolean isMayuscula = true;

    public EventoTecladoPresion(JTextComponent t) {
        componente = t;
    }

    public EventoTecladoPresion(JTextComponent t, int max) {
        componente = t;
        this.max = max;
    }

    public EventoTecladoPresion(JTextComponent t, int max, boolean isMayuscula) {
        componente = t;
        this.max = max;
        this.isMayuscula = isMayuscula;
    }

    public EventoTecladoPresion(JTextComponent t, int min, int max) {
        componente = t;
        this.min = min;
        this.max = max;
    }

    public void keyTyped(KeyEvent e) {
        super.keyTyped(e);
        super.keyTyped(e);
        char caracter = e.getKeyChar();
        if (this.max == 0 || (componente.getText().length() < this.max)) {
            if (((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE && ((caracter != '/')))) {
                e.consume();  // ignorar el evento de teclado            
            }
            if (componente.getText().length() == 0 && (caracter == '/')) {
                e.consume();
            }
            int cont = 0;
            for (int i = 0; i < componente.getText().length(); i++) {
                if ((componente.getText().charAt(i) == '/')) {
                    cont++;
                }
            }
            if (cont > 0 && (caracter == '/')) {
                e.consume();
            }
        } else {
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        super.keyReleased(e); //To change body of generated methods, choose Tools | Templates.
        if (isMayuscula) {
            char caracter = e.getKeyChar();
            int car = (int) caracter;

            if (!(KeyEvent.VK_LEFT == car || KeyEvent.VK_RIGHT == car || car == 65535 || car == 32 || car == 8 || car == 127)) {
                componente.setText(componente.getText().toUpperCase());
            }
        }
        //   componente.setText(componente.getText().toUpperCase());
    }
}
