package vista.utilidades.validadores;

import java.awt.event.KeyAdapter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author janeth
 */
public class EventoTecladoSoloMayusculas extends KeyAdapter {

    private JTextComponent componente;
    private int min = 0;
    private int max = 0;

    public EventoTecladoSoloMayusculas(JTextComponent t) {
        componente = t;
    }

    public EventoTecladoSoloMayusculas(JTextComponent t, int max) {
        componente = t;
        this.max = max;
    }

    public EventoTecladoSoloMayusculas(JTextComponent t, int min, int max) {
        componente = t;
        this.min = min;
        this.max = max;
    }

    public void keyTyped(KeyEvent e) {
        super.keyTyped(e);
        if (this.max == 0 || (componente.getText().length() < this.max)) {
        } else {
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        super.keyReleased(e); //To change body of generated methods, choose Tools | Templates.
        //componente.setText(componente.getText().toUpperCase());
        char caracter = e.getKeyChar();
        int car = (int) caracter;

        if (!(KeyEvent.VK_LEFT == car || KeyEvent.VK_RIGHT == car || car == 65535 || car == 32 || car == 8 || car == 127)) {
            componente.setText(componente.getText().toUpperCase());
        }
    }

}
