package vista.utilidades.validadores;

import com.sun.webkit.event.WCKeyEvent;
import java.awt.event.KeyAdapter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 * @author janeth
 */
public class EventoTecladoLetras extends KeyAdapter {

    private JTextComponent componente;
    private int min = 0;
    private int max = 0;

    public EventoTecladoLetras(JTextComponent t) {
        componente = t;
    }

    public EventoTecladoLetras(JTextComponent t, int max) {
        componente = t;
        this.max = max;
    }

    public EventoTecladoLetras(JTextComponent t, int min, int max) {
        componente = t;
        this.min = min;
        this.max = max;
    }

    public void keyTyped(KeyEvent e) {
        super.keyTyped(e);

        if (this.max == 0 || (componente.getText().length() < this.max)) {
            char caracter = e.getKeyChar();
            System.out.println(((int) (caracter)));
            if (Character.isDigit(caracter)) {
                e.consume();  // ignorar el evento de teclado            
            } else if (((int) e.getKeyChar() > 32 && (int) e.getKeyChar() <= 47)
                    || ((int) e.getKeyChar() > 58 && (int) e.getKeyChar() <= 64)
                    || ((int) e.getKeyChar() > 91 && (int) e.getKeyChar() <= 96)
                    || ((int) e.getKeyChar() > 123 && (int) e.getKeyChar() <= 240)
                    || ((int) e.getKeyChar() > 242 && (int) e.getKeyChar() <= 255)) {
                e.consume();
            }
        } else {
            e.consume();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        char caracter = e.getKeyChar();
        int car = (int) caracter;

        if (!(KeyEvent.VK_LEFT == car || KeyEvent.VK_RIGHT == car || car == 65535 || car == 32 || car == 8 || car == 127)) {
            componente.setText(componente.getText().toUpperCase());
        }
        //super.keyReleased(e); //To change body of generated methods, choose Tools | Templates.

    }

}
