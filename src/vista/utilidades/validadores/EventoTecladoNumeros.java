package vista.utilidades.validadores;

import java.awt.event.KeyAdapter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.event.KeyEvent;
import javax.swing.text.JTextComponent;

/**
 *
 * @author janeth
 */
public class EventoTecladoNumeros extends KeyAdapter {

    private JTextComponent componente;
    private int min = 0;
    private int max = 0;

    public EventoTecladoNumeros(JTextComponent t) {
        componente = t;
    }

    public EventoTecladoNumeros(JTextComponent t, int max) {
        componente = t;
        this.max = max;
    }

    public EventoTecladoNumeros(JTextComponent t, int min, int max) {
        componente = t;
        this.min = min;
        this.max = max;
    }

    public void keyTyped(KeyEvent e) {
        super.keyTyped(e);
        char caracter = e.getKeyChar();
        if (this.max == 0 || (componente.getText().length() < this.max)) {
            if (((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE && ((caracter != '.')))) {
                e.consume();  // ignorar el evento de teclado            
            }
            if (componente.getText().length() == 0 && (caracter == '.')) {
                e.consume();
            }
            int cont = 0;
            for (int i = 0; i < componente.getText().length(); i++) {
                if ((componente.getText().charAt(i) == '.')) {
                    cont++;
                }
            }
            if (cont > 0 && (caracter == '.')) {
                e.consume();
            }
        } else {
            e.consume();
        }
    }

}
