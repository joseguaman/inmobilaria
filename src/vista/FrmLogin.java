/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.servicios.CuentaService;
import controlador.utilidades.Sesion;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import vista.utilidades.UtilidadesConponente;

/**
 *
 * @author sissysebas
 */
public class FrmLogin extends javax.swing.JDialog {

    /**
     * Creates new form FrmLogin
     */
    public FrmLogin(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    private void inicioSesion() {
        if(!UtilidadesConponente.mostrarError(txt_user, "Falta este campo", 'r')
        && !UtilidadesConponente.mostrarError(txt_clave, "Falta este campo", 'r')) {
            Sesion.setCuenta(new CuentaService().inicioSesion(txt_user.getText(), 
                new String(txt_clave.getPassword())));
            if(Sesion.getCuenta() != null) {
                FrmPrincipal.ROL = Sesion.getCuenta().getPersona().getRol().getNombre();
                new FrmPrincipal().setVisible(true);
                dispose();
            } else {
                UtilidadesConponente
                  .mensajeError("Error de acceso", "Hubo un error con sus credenciales");
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        txt_clave = new org.edisoncor.gui.passwordField.PasswordFieldRound();
        panelImage2 = new org.edisoncor.gui.panel.PanelImage();
        txt_user = new org.edisoncor.gui.textField.TextFieldRound();
        btn_salir = new org.edisoncor.gui.button.ButtonAeroRight();
        btn_icion = new org.edisoncor.gui.button.ButtonAeroLeft();
        labelCustom1 = new org.edisoncor.gui.label.LabelCustom();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("INICIO DE SESION");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(null);

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/lock.png"))); // NOI18N
        jPanel2.add(panelImage1);
        panelImage1.setBounds(30, 170, 30, 30);

        txt_clave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_claveActionPerformed(evt);
            }
        });
        jPanel2.add(txt_clave);
        txt_clave.setBounds(70, 170, 180, 30);

        panelImage2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/user.png"))); // NOI18N
        jPanel2.add(panelImage2);
        panelImage2.setBounds(30, 100, 30, 30);

        txt_user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_userActionPerformed(evt);
            }
        });
        jPanel2.add(txt_user);
        txt_user.setBounds(70, 100, 180, 30);

        btn_salir.setBackground(new java.awt.Color(229, 14, 14));
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });
        jPanel2.add(btn_salir);
        btn_salir.setBounds(150, 240, 120, 29);

        btn_icion.setBackground(new java.awt.Color(31, 157, 76));
        btn_icion.setText("Inicio sesion");
        btn_icion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_icionActionPerformed(evt);
            }
        });
        jPanel2.add(btn_icion);
        btn_icion.setBounds(20, 240, 110, 29);

        labelCustom1.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom1.setText("INICIO DE SESION");
        jPanel2.add(labelCustom1);
        labelCustom1.setBounds(10, 10, 270, 50);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 290, 300);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 290, 300);

        setSize(new java.awt.Dimension(302, 330));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_icionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_icionActionPerformed
        // TODO add your handling code here:
        inicioSesion();
    }//GEN-LAST:event_btn_icionActionPerformed

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btn_salirActionPerformed

    private void txt_userActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_userActionPerformed
        // TODO add your handling code here:
        inicioSesion();
    }//GEN-LAST:event_txt_userActionPerformed

    private void txt_claveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_claveActionPerformed
        // TODO add your handling code here:
        inicioSesion();
    }//GEN-LAST:event_txt_claveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
            
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
        } 
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmLogin dialog = new FrmLogin(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonAeroLeft btn_icion;
    private org.edisoncor.gui.button.ButtonAeroRight btn_salir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private org.edisoncor.gui.label.LabelCustom labelCustom1;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private org.edisoncor.gui.panel.PanelImage panelImage2;
    private org.edisoncor.gui.passwordField.PasswordFieldRound txt_clave;
    private org.edisoncor.gui.textField.TextFieldRound txt_user;
    // End of variables declaration//GEN-END:variables
}
