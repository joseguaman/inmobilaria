/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tablas;

import controlador.servicios.PagoService;
import controlador.servicios.PersonaService;
import controlador.utilidades.Utilidades;
import java.util.ArrayList;
import java.util.List;
import modelo.Pago;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class DataPago {
    public String cliente;
    public String propietario;
    public String cedula;
    public String nroContrato;
    public String propiedad;
    public String fechaPago;
    public Long idContrato;
    public Long id;
    public Boolean estado;
    public Double pago;
    public String fechaLetras;
    public List<DataPago> lista = new ArrayList<>();
    public List<DataPago> cargarLista() {
        PagoService pagoService = new PagoService();
        PersonaService personaService = new PersonaService();
        List<Pago> listaPago = pagoService.listarPagoEstado(true);
        for(Pago pago: listaPago) {
            DataPago data = new DataPago();
            Persona persona = personaService.getPersonaExternal(pago.getContrato().getExternalIdArrendatario());
            data.cedula = persona.getCedula();
            data.cliente = persona.getApellidos() + " " + persona.getNombres();
            data.estado = pago.getEstado();
            data.propietario = pago.getContrato().getInmueble().getPersona().toString();
            data.fechaPago = Utilidades.formatearFechaSimple(pago.getFechaPago());
            data.id = pago.getId();
            data.idContrato = pago.getContrato().getId();
            data.nroContrato = pago.getContrato().getNumeroContrato();
            data.propiedad = pago.getContrato().getInmueble().toString();
            data.pago = pago.getValor();
            data.fechaLetras = pago.getMesPago();
            lista.add(data);
            
        }
        return lista;
    }
    
    public List<DataPago> cargarListaPagado() {
        PagoService pagoService = new PagoService();
        PersonaService personaService = new PersonaService();
        List<Pago> listaPago = pagoService.listarPagoEstado(false);
        for(Pago pago: listaPago) {
            DataPago data = new DataPago();
            Persona persona = personaService.getPersonaExternal(pago.getContrato().getExternalIdArrendatario());
            data.cedula = persona.getCedula();
            data.cliente = persona.toString();
            data.estado = pago.getEstado();
            data.fechaPago = Utilidades.formatearFechaSimple(pago.getFechaPago());
            data.id = pago.getId();
            data.propietario = pago.getContrato().getInmueble().getPersona().toString();
            data.idContrato = pago.getContrato().getId();
            data.nroContrato = pago.getContrato().getNumeroContrato();
            data.propiedad = pago.getContrato().getInmueble().toString();
            data.pago = pago.getValor();
            data.fechaLetras = pago.getMesPago();
            lista.add(data);
        }
        return lista;
    }
    
    public List<DataPago> cargarListaPagado(List<Pago> listaPago) {
        PagoService pagoService = new PagoService();
        PersonaService personaService = new PersonaService();
        //List<Pago> listaPago = pagoService.listarPagoEstado(false);
        for(Pago pago: listaPago) {
            DataPago data = new DataPago();
            Persona persona = personaService.getPersonaExternal(pago.getContrato().getExternalIdArrendatario());
            data.cedula = persona.getCedula();
            data.cliente = persona.toString();
            data.estado = pago.getEstado();
            data.fechaPago = Utilidades.formatearFechaSimple(pago.getFechaPago());
            data.id = pago.getId();
            data.propietario = pago.getContrato().getInmueble().getPersona().toString();
            data.idContrato = pago.getContrato().getId();
            data.nroContrato = pago.getContrato().getNumeroContrato();
            data.propiedad = pago.getContrato().getInmueble().toString();
            data.pago = pago.getValor();
            data.fechaLetras = pago.getMesPago();
            lista.add(data);
        }
        return lista;
    }
}
