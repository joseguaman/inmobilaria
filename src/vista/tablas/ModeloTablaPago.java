/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tablas;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sissysebas
 */
public class ModeloTablaPago extends AbstractTableModel {
    private List<DataPago> lista = new ArrayList<>();

    public List<DataPago> getLista() {
        return lista;
    }

    public void setLista(List<DataPago> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DataPago data = lista.get(rowIndex);
        switch(columnIndex) {
            case 0: return data.cedula;
            case 1: return data.cliente;
            case 2: return data.propietario;
            case 3: return data.nroContrato;
            case 4: return data.propiedad;
            case 5: return data.fechaLetras;
            case 6: return data.pago;
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Cedula";
            case 1: return "Cliente";
            case 2: return "Propietario";
            case 3: return "Nro Contrato";
            case 4: return "Propiedad";
            case 5: return "Fecha de Pago";
            case 6: return "Valor";
            default: return null;
        }
    }
    
}
