/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tablas;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import lombok.Getter;
import lombok.Setter;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class NodeloTablaPersona extends AbstractTableModel {
    @Getter
    @Setter
    private List<Persona> lista = new ArrayList<>();

    @Override
    public int getColumnCount() {
        return 6;
    }
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Persona p = lista.get(rowIndex);
        switch(columnIndex) {
            case 0: return p.getCedula();
            case 1: return p.toString();
            case 2: return p.getDireccion();
            case 3: return p.getTelefono();
            case 4: return p.getRol().getNombre();
            case 5: return (p.isEstado()) ? "Activo" : "Inactivo";
            default: return null; 
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "cedula";
            case 1: return "Propietario/Cliente";
            case 2: return "Direccion";
            case 3: return "Telefono";
            case 4: return "Tipo";
            case 5: return "Estado";
            default: return null; 
        }
        
    }
    
    
    
    
}











