/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tablas;

import controlador.servicios.PersonaService;
import controlador.utilidades.Utilidades;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import lombok.Getter;
import lombok.Setter;
import modelo.Contrato;
import modelo.Persona;

/**
 *
 * @author sissysebas
 */
public class ModeloTablaContrato extends AbstractTableModel {
    @Getter
    @Setter
    private List<Contrato> lista = new ArrayList<>();

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contrato contrato = lista.get(rowIndex);
        Persona p = new PersonaService().getPersonaExternal(contrato.getExternalIdArrendatario());
        switch(columnIndex) {
            case 0: return p.toString();
            case 1: return contrato.getNumeroContrato();
            case 2: return contrato.getInmueble().toString();
            case 3: return Utilidades.formatearFechaSimple(contrato.getFechaInicio());
            case 4: return Utilidades.formatearFechaSimple(contrato.getFechaFinalizacion());
            case 5: return contrato.getGarantia();
            case 6: return contrato.getDuracion();
            case 7: return (contrato.getFinalizacion() != null) ? contrato.getFinalizacion().getMotivo() : "VIGENTE";
            case 8: return contrato.getPrecio();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Cliente";
            case 1: return "Contrato";
            case 2: return "Inmueble";
            case 3: return "Fecha Inicio";
            case 4: return "Fecha Finalizacion";
            case 5: return "Garantia";
            case 6: return "Duracion";
            case 7: return "Fecha Finalizacion";
            case 8: return "Precio";
            default: return null;
        }
    }
    
}
