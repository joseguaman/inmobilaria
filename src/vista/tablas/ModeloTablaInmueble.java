/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.tablas;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import lombok.Getter;
import lombok.Setter;
import modelo.Inmueble;

/**
 *
 * @author sissysebas
 */
public class ModeloTablaInmueble extends AbstractTableModel {
    @Getter
    @Setter
    private List<Inmueble> lista = new ArrayList<>();

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Inmueble inmueble = lista.get(rowIndex);
        switch(columnIndex) {
            case 0: return inmueble.getPersona().toString();
            case 1: return inmueble.getTipo();
            case 2: return (inmueble.getEstado()) ? "SI" : "NO";
            case 3: return inmueble.getNroPisos();
            case 4: return inmueble.getNroHabitaciones();
            case 5: return inmueble.getCiudad() + " " + inmueble.getBarrio()+" : " + inmueble.getCallePrincipal() + ", " + inmueble.getCalleSecundaria();
            case 6: return inmueble.getPrecio();
            case 7: return (inmueble.getActivo_desactivo() != null && inmueble.getActivo_desactivo()) ? "Activo" : "Inactivo";
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Propietario";
            case 1: return "Tipo";
            case 2: return "Prestado";
            case 3: return "nro Pisos";
            case 4: return "Nro habitacion";
            case 5: return "Direccion";
            case 6: return "Precio";
            case 7: return "Estado";
            default: return null;
        }
    }
    
    
    
}
