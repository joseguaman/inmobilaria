/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import controlador.servicios.InmuebleService;
import vista.tablas.DataPago;

/**
 *
 * @author sissysebas
 */
public class Frm_MostrarDatosPago extends javax.swing.JDialog {
    private DataPago pago;
    public static int seleccion = 0;
    /** Creates new form Frm_MostrarDatosInmueble */
    public Frm_MostrarDatosPago(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();        
    }
    public Frm_MostrarDatosPago(java.awt.Frame parent, boolean modal, DataPago data) {
        super(parent, modal);
        initComponents();        
        this.pago = data;
        Frm_MostrarDatosPago.seleccion = 0;
        cargarDatos();
    }
    private void cargarDatos() {
        lblnrocontrato.setText(pago.nroContrato);
        lblfecha.setText(pago.fechaPago);
        lblvalor.setText(String.valueOf(pago.pago));
        lblnrocliente.setText(pago.cliente);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        balloonManager1 = new org.edisoncor.gui.util.BalloonManager();
        jPanel1 = new javax.swing.JPanel();
        labelCustom2 = new org.edisoncor.gui.label.LabelCustom();
        labelMetric1 = new org.edisoncor.gui.label.LabelMetric();
        labelMetric2 = new org.edisoncor.gui.label.LabelMetric();
        labelMetric3 = new org.edisoncor.gui.label.LabelMetric();
        buttonAeroRight1 = new org.edisoncor.gui.button.ButtonAeroRight();
        lblvalor = new javax.swing.JLabel();
        lblnrocontrato = new javax.swing.JLabel();
        lblfecha = new javax.swing.JLabel();
        labelMetric4 = new org.edisoncor.gui.label.LabelMetric();
        lblnrocliente = new javax.swing.JLabel();
        buttonAeroLeft1 = new org.edisoncor.gui.button.ButtonAeroLeft();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        labelCustom2.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom2.setText("DATOS DEL PAGO");
        jPanel1.add(labelCustom2);
        labelCustom2.setBounds(10, 10, 380, 50);

        labelMetric1.setText("Valor:");
        jPanel1.add(labelMetric1);
        labelMetric1.setBounds(10, 210, 110, 17);

        labelMetric2.setText("Nro Contrato:");
        jPanel1.add(labelMetric2);
        labelMetric2.setBounds(10, 90, 110, 17);

        labelMetric3.setText("Fecha:");
        jPanel1.add(labelMetric3);
        labelMetric3.setBounds(10, 170, 110, 17);

        buttonAeroRight1.setBackground(new java.awt.Color(229, 14, 14));
        buttonAeroRight1.setText("CANCELAR");
        buttonAeroRight1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight1ActionPerformed(evt);
            }
        });
        jPanel1.add(buttonAeroRight1);
        buttonAeroRight1.setBounds(240, 260, 87, 29);

        lblvalor.setText("Ninguno");
        jPanel1.add(lblvalor);
        lblvalor.setBounds(140, 220, 240, 16);

        lblnrocontrato.setText("Ninguno");
        jPanel1.add(lblnrocontrato);
        lblnrocontrato.setBounds(140, 90, 240, 16);

        lblfecha.setText("Ninguno");
        jPanel1.add(lblfecha);
        lblfecha.setBounds(140, 180, 240, 16);

        labelMetric4.setText("Cliente:");
        jPanel1.add(labelMetric4);
        labelMetric4.setBounds(10, 130, 110, 17);

        lblnrocliente.setText("Ninguno");
        jPanel1.add(lblnrocliente);
        lblnrocliente.setBounds(140, 130, 240, 16);

        buttonAeroLeft1.setBackground(new java.awt.Color(31, 157, 76));
        buttonAeroLeft1.setText("PAGAR");
        buttonAeroLeft1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroLeft1ActionPerformed(evt);
            }
        });
        jPanel1.add(buttonAeroLeft1);
        buttonAeroLeft1.setBounds(30, 260, 110, 29);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 400, 300);

        setSize(new java.awt.Dimension(410, 334));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAeroRight1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight1ActionPerformed
        // TODO add your handling code here:
        Frm_MostrarDatosPago.seleccion = 0;
        dispose();
    }//GEN-LAST:event_buttonAeroRight1ActionPerformed

    private void buttonAeroLeft1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroLeft1ActionPerformed
        // TODO add your handling code here:
        Frm_MostrarDatosPago.seleccion = 1;
        dispose();
    }//GEN-LAST:event_buttonAeroLeft1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_MostrarDatosPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_MostrarDatosPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_MostrarDatosPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_MostrarDatosPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Frm_MostrarDatosPago dialog = new Frm_MostrarDatosPago(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.util.BalloonManager balloonManager1;
    private org.edisoncor.gui.button.ButtonAeroLeft buttonAeroLeft1;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight1;
    private javax.swing.JPanel jPanel1;
    private org.edisoncor.gui.label.LabelCustom labelCustom2;
    private org.edisoncor.gui.label.LabelMetric labelMetric1;
    private org.edisoncor.gui.label.LabelMetric labelMetric2;
    private org.edisoncor.gui.label.LabelMetric labelMetric3;
    private org.edisoncor.gui.label.LabelMetric labelMetric4;
    private javax.swing.JLabel lblfecha;
    private javax.swing.JLabel lblnrocliente;
    private javax.swing.JLabel lblnrocontrato;
    private javax.swing.JLabel lblvalor;
    // End of variables declaration//GEN-END:variables

}
