/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.servicios.ContratoService;
import controlador.servicios.InmuebleService;
import controlador.servicios.PersonaService;
import controlador.utilidades.Utilidades;
import java.io.File;
import java.util.Date;
import java.util.UUID;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import vista.tablas.ModeloTablaContrato;
import vista.utilidades.UtilidadesConponente;
import vista.utilidades.validadores.EventoTecladoLetrasNumeros;
import vista.utilidades.validadores.EventoTecladoNumeros;

/**
 *
 * @author sissysebas
 */
public class FrmContrato extends javax.swing.JDialog {

    private PersonaService personaService = new PersonaService();
    private InmuebleService inmuebleService = new InmuebleService();
    private ModeloTablaContrato modelo = new ModeloTablaContrato();
    private ContratoService contratoService = new ContratoService();
    private String dir = "";

    /**
     * Creates new form FrmCliente
     */
    public FrmContrato(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        limpiar();
        txt_garantia.addKeyListener(new EventoTecladoNumeros(txt_garantia));
        txt_precio.addKeyListener(new EventoTecladoNumeros(txt_precio));
        txt_observaciones.addKeyListener(new EventoTecladoLetrasNumeros(txt_observaciones));
        buttonGroup1.add(radio_todos);
        buttonGroup1.add(radio_vigente);
        buttonGroup1.add(raio_caducados);
        //buttonGroup1.add(raio_cliente);
        //buttonGroup1.add(radio_propietario);
    }

    private void cargarTabla() {
        modelo.setLista(contratoService.todos());
        tbl_tabla.setModel(modelo);
        tbl_tabla.updateUI();
    }

    private void limpiar() {

        txt_busqueda.setText("");
        txt_cedula.setText("");
        txt_cliente.setText("");
        txt_codigo.setText("");
        txt_contrato.setText("");
        dir = "";
        txt_fechaI.setDate(new Date());
        txt_fono.setText("");
        txt_garantia.setText("");
        txt_observaciones.setText("");
        txt_precio.setText("");
        UtilidadesConponente.llenarComboInmueblesLibres(cbx_inmueble, inmuebleService.listarInmuebleEstado(false));
        inmuebleService.fijarInmueble(null);
        personaService.fijarPersona(null);
        contratoService.fijarContrato(null);
        codigoInmueble();
        cargarTabla();
    }

    private void filtrar() {
        if (radio_vigente.isSelected()) {
            modelo.setLista(contratoService.listarContratoActivos());
        } else if (raio_caducados.isSelected()) {
            modelo.setLista(contratoService.listarContratoDesactivos());
        } else {
            modelo.setLista(contratoService.todos());
        }
        tbl_tabla.setModel(modelo);
        tbl_tabla.updateUI();
    }

    public PersonaService getPersonaService() {
        return personaService;
    }

    private void codigoInmueble() {
        String tipo = "Contrato-";
        for (int i = 0; i < 5; i++) {
            tipo += "0";
        }
        tipo += (contratoService.todos().size() + 1);
        txt_codigo.setText(tipo);
    }

    private void buscarPersona() {
        //if(!txt_cedula.getText().trim().isEmpty()) {
        new FrmCliente(null, true, this).setVisible(true);
        //PersonaService aux = new PersonaService();
        //personaService.fijarPersona(personaService.getPersonaCedula(txt_cedula.getText()));
        if (personaService.getPersona().getId() != null) {
            txt_cedula.setText(personaService.getPersona().getCedula());
            txt_cliente.setText(personaService.getPersona().toString());
            txt_fono.setText(personaService.getPersona().getTelefono());
        } else {
            txt_cedula.setText("");
            txt_cliente.setText("");
            txt_fono.setText("");
        }

        //} else {
        //   UtilidadesConponente.mensajeError("Error", "Ingrese la cedula");
        //}
    }

    private void cargarPrecio() {
        try {
            txt_precio.setText(String.valueOf(UtilidadesConponente.obtenerInmueble(cbx_inmueble).getPrecio()));
        } catch (Exception e) {
        }
    }

    private void subirArchivo() {
        if (!dir.isEmpty()) {
            String aux = Utilidades.getRutaContratos() + File.separatorChar + contratoService.getContrato().getExternal_id() + "-compra.pdf";
            Utilidades.copiararchivos(dir, aux);
            //ServicioArchivo sa = new ServicioArchivo();
            contratoService.getContrato().setArchivo(dir);
            contratoService.guardar();
            //sa.getArchivo().setDireccion_arch(Utilidades.obtenerRutaRoot() + File.separatorChar + "compras" + File.separatorChar);
            //sa.getArchivo().setNombre_arch(this.sv.getCompra().getId_compra().intValue() + "-compra.pdf");
            //sa.getArchivo().setCompra(sv.getCompra());
            //sv.getCompra().setArchivo(sa.getArchivo());
        } else {
            //UtilidadesConponente.mensajeError("Error", "No ha cargado el archivo");
            //JOptionPane.showMessageDialog(this, "No ha cargado el archivo", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void cargarFileChooser() {
        // TODO add your handling code here:
        //subirArchivo();
        JFileChooser choser = new JFileChooser();
        choser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                //if (f.isDirectory()) {
                //   return true;
                //} else {
                return f.getName().toLowerCase().endsWith(".pdf");
                //}
            }

            @Override
            public String getDescription() {
                return "Documentos pdf (*.pdf)";
            }
        });
        choser.setAcceptAllFileFilterUsed(true);
        int aux = choser.showOpenDialog(this);
        if (aux == JFileChooser.APPROVE_OPTION && choser.getSelectedFile().getPath().endsWith(".pdf")) {
            dir = choser.getSelectedFile().getPath();
            txt_contrato.setText(choser.getSelectedFile().getName());
        } else {
            UtilidadesConponente.mensajeError("Error", "Solo se aceptan archivos en formato pdf");
        }
    }

    private void cargarObjeto() {
        contratoService.getContrato().setDuracion(cbx_duracion.getSelectedItem().toString());
        contratoService.getContrato().setExternalIdArrendatario(personaService.getPersona().getExternal_id());
        contratoService.getContrato().setExternal_id(UUID.randomUUID().toString());
        contratoService.getContrato().setFechaInicio(txt_fechaI.getDate());
        contratoService.getContrato().setGarantia(Double.parseDouble(txt_garantia.getText()));
        contratoService.getContrato().setPrecio(Double.parseDouble(txt_precio.getText()));
        contratoService.getContrato().setInmueble(UtilidadesConponente.obtenerInmueble(cbx_inmueble));
        contratoService.getContrato().setNumeroContrato(txt_codigo.getText());
        contratoService.getContrato().setObservaciones(txt_observaciones.getText());
        contratoService.getContrato().setFechaFinalizacion(Utilidades.sumarMesesAnios(cbx_duracion.getSelectedItem().toString(), txt_fechaI.getDate()));
        contratoService.getContrato().setExternalIdArrendatario(personaService.getPersona().getExternal_id());

    }

    private void guardar() {
        if (!UtilidadesConponente.mostrarError(txt_cedula, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_cliente, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_fono, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_fechaI, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_contrato, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_observaciones, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_garantia, "Falta este campo", 'r')
                && !UtilidadesConponente.mostrarError(txt_precio, "Falta este campo", 'r')) {
            if (!UtilidadesConponente.mostrarError(txt_garantia, "Campo numerico", 'n')
                    && !UtilidadesConponente.mostrarError(txt_precio, "Campo numerico", 'n')) {
                cargarObjeto();
                if (contratoService.getContrato().getId() != null) {
                    //modificar
                    if (this.contratoService.guardar()) {
                        this.subirArchivo();
                        UtilidadesConponente.mensajeOk("Se ha editado", "Se ha editado los datos del contrato correctamente");
                        inmuebleService.fijarInmueble(UtilidadesConponente.obtenerInmueble(cbx_inmueble));
                        inmuebleService.getInmueble().setEstado(false);
                        inmuebleService.guardar();
                        limpiar();
                    } else {
                        //  JOptionPane.showMessageDialog(this, "No se pudo guardar", "Error", JOptionPane.ERROR_MESSAGE);
                        UtilidadesConponente.mensajeError("Error", "No se pudo editar el contrato");
                    }
                } else {
                    //guardar
                    if (!dir.trim().isEmpty()) {
                        int i = JOptionPane.showConfirmDialog(this, "Esta seguro de registrar este contrato");
                        if (i == JOptionPane.OK_OPTION) {
                            if (!contratoService.getContrato().getInmueble().getEstado()) {
                                contratoService.fijarContrato(UtilidadesConponente.crearCuotas(contratoService.getContrato()));
                                if (this.contratoService.guardar()) {
                                    inmuebleService.fijarInmueble(contratoService.getContrato().getInmueble());
                                    inmuebleService.getInmueble().setEstado(Boolean.TRUE);
                                    inmuebleService.guardar();
                                    this.subirArchivo();
                                    //InmuebleService inmuebleService = new InmuebleService();
                                    //inmuebleService
                                    
                                    UtilidadesConponente.mensajeOk("Se ha registrado", "Se ha registrado el contrato correctamente");
                                    limpiar();
                                } else {
                                    //  JOptionPane.showMessageDialog(this, "No se pudo guardar", "Error", JOptionPane.ERROR_MESSAGE);
                                    UtilidadesConponente.mensajeError("Error", "No se pudo registrar el contrato");
                                }
                            } else {
                                UtilidadesConponente.mensajeError("Error", "No se pudo registrar el contrato, el inmueble no esta libre");
                            }
                        }
                    } else {
                        UtilidadesConponente.mensajeError("Error", "No se pudo registrar el contrato, fata subir el pdf");
                    }
                }
            }
        }
    }

    private void cargarVista() {
        txt_cedula.setText(personaService.getPersona().getCedula());
        txt_cliente.setText(personaService.getPersona().toString());
        txt_codigo.setText(contratoService.getContrato().getNumeroContrato());
        txt_contrato.setText(contratoService.getContrato().getArchivo());
        txt_fechaI.setDate(contratoService.getContrato().getFechaInicio());
        txt_fono.setText(personaService.getPersona().getTelefono());
        txt_garantia.setText(String.valueOf(contratoService.getContrato().getGarantia()));
        txt_precio.setText(String.valueOf(contratoService.getContrato().getPrecio()));
        txt_observaciones.setText(contratoService.getContrato().getObservaciones());
        cbx_duracion.setSelectedItem(contratoService.getContrato().getDuracion());
        cbx_inmueble.setSelectedItem(inmuebleService.getInmueble());
    }

    private void editar() {
        int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            if (modelo.getLista().get(fila).getFechaFinalizacion() != null) {
                contratoService.fijarContrato(modelo.getLista().get(fila));
                personaService.fijarPersona(personaService.getPersonaExternal(contratoService.getContrato().getExternalIdArrendatario()));
                inmuebleService.fijarInmueble(contratoService.getContrato().getInmueble());
                cargarVista();
            } else {
                UtilidadesConponente.mensajeError("Error", "Seleccione un contrato que este en vigencia");
            }
        } else {
            UtilidadesConponente.mensajeError("Error", "Seleccione un contrato de la tabla");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        panelReflect1 = new org.edisoncor.gui.panel.PanelReflect();
        buttonAeroLeft1 = new org.edisoncor.gui.button.ButtonAeroLeft();
        jLabel2 = new javax.swing.JLabel();
        txt_fono = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel4 = new javax.swing.JLabel();
        buttonAeroRight2 = new org.edisoncor.gui.button.ButtonAeroRight();
        jLabel6 = new javax.swing.JLabel();
        buttonAeroRight1 = new org.edisoncor.gui.button.ButtonAeroRight();
        labelCustom1 = new org.edisoncor.gui.label.LabelCustom();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        buttonAeroRight4 = new org.edisoncor.gui.button.ButtonAeroRight();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cbx_inmueble = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jLabel10 = new javax.swing.JLabel();
        buttonAeroRight5 = new org.edisoncor.gui.button.ButtonAeroRight();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_observaciones = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        txt_fechaI = new com.toedter.calendar.JDateChooser();
        cbx_duracion = new org.edisoncor.gui.comboBox.ComboBoxRect();
        txt_cedula = new org.edisoncor.gui.textField.TextFieldRound();
        txt_contrato = new org.edisoncor.gui.textField.TextFieldRound();
        txt_codigo = new org.edisoncor.gui.textField.TextFieldRound();
        txt_precio = new org.edisoncor.gui.textField.TextFieldRound();
        txt_cliente = new org.edisoncor.gui.textField.TextFieldRound();
        txt_garantia = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel13 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_busqueda = new org.edisoncor.gui.textField.TextFieldRound();
        radio_vigente = new javax.swing.JRadioButton();
        raio_caducados = new javax.swing.JRadioButton();
        radio_todos = new javax.swing.JRadioButton();
        labelCustom2 = new org.edisoncor.gui.label.LabelCustom();
        buttonAeroRight6 = new org.edisoncor.gui.button.ButtonAeroRight();
        buttonAeroLeft2 = new org.edisoncor.gui.button.ButtonAeroLeft();
        buttonCircle1 = new org.edisoncor.gui.button.ButtonCircle();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(null);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(20, 150, 610, 130);

        panelReflect1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect1.setLayout(null);

        buttonAeroLeft1.setBackground(new java.awt.Color(31, 157, 76));
        buttonAeroLeft1.setText("Guardar");
        buttonAeroLeft1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroLeft1ActionPerformed(evt);
            }
        });
        panelReflect1.add(buttonAeroLeft1);
        buttonAeroLeft1.setBounds(200, 440, 110, 29);

        jLabel2.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel2.setText("Cedula:");
        panelReflect1.add(jLabel2);
        jLabel2.setBounds(30, 140, 80, 16);

        txt_fono.setEnabled(false);
        panelReflect1.add(txt_fono);
        txt_fono.setBounds(410, 170, 180, 30);

        jLabel4.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel4.setText("Telefono:");
        panelReflect1.add(jLabel4);
        jLabel4.setBounds(330, 180, 80, 16);

        buttonAeroRight2.setBackground(new java.awt.Color(229, 14, 14));
        buttonAeroRight2.setText("Cancelar");
        panelReflect1.add(buttonAeroRight2);
        buttonAeroRight2.setBounds(320, 440, 120, 29);

        jLabel6.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel6.setText("Cliente:");
        panelReflect1.add(jLabel6);
        jLabel6.setBounds(30, 170, 80, 16);

        buttonAeroRight1.setBackground(new java.awt.Color(14, 62, 229));
        buttonAeroRight1.setText("BUSCAR");
        buttonAeroRight1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight1ActionPerformed(evt);
            }
        });
        panelReflect1.add(buttonAeroRight1);
        buttonAeroRight1.setBounds(510, 130, 80, 29);

        labelCustom1.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom1.setText("DATOS DEL CONTRATO");
        panelReflect1.add(labelCustom1);
        labelCustom1.setBounds(10, 10, 600, 50);

        jLabel7.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel7.setText("Codigo:");
        panelReflect1.add(jLabel7);
        jLabel7.setBounds(330, 70, 80, 16);

        jLabel3.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel3.setText("Inmueble:");
        panelReflect1.add(jLabel3);
        jLabel3.setBounds(30, 250, 80, 16);

        buttonAeroRight4.setBackground(new java.awt.Color(14, 62, 229));
        buttonAeroRight4.setText("CARGAR");
        buttonAeroRight4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight4ActionPerformed(evt);
            }
        });
        panelReflect1.add(buttonAeroRight4);
        buttonAeroRight4.setBounds(250, 400, 80, 29);
        panelReflect1.add(jSeparator1);
        jSeparator1.setBounds(10, 110, 590, 10);
        panelReflect1.add(jSeparator2);
        jSeparator2.setBounds(10, 220, 590, 10);

        jLabel8.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel8.setText("Observa:");
        panelReflect1.add(jLabel8);
        jLabel8.setBounds(30, 330, 110, 16);

        jLabel9.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel9.setText("Duracion:");
        panelReflect1.add(jLabel9);
        jLabel9.setBounds(330, 290, 80, 16);

        cbx_inmueble.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "3 meses", "6 meses", "9 meses", "12 meses", "15 meses", "18 meses", "21 meses", "24 meses" }));
        cbx_inmueble.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbx_inmuebleItemStateChanged(evt);
            }
        });
        panelReflect1.add(cbx_inmueble);
        cbx_inmueble.setBounds(110, 240, 360, 30);

        jLabel10.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel10.setText("Contrato:");
        panelReflect1.add(jLabel10);
        jLabel10.setBounds(30, 400, 80, 16);

        buttonAeroRight5.setBackground(new java.awt.Color(14, 62, 229));
        buttonAeroRight5.setText("DETALLES");
        buttonAeroRight5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight5ActionPerformed(evt);
            }
        });
        panelReflect1.add(buttonAeroRight5);
        buttonAeroRight5.setBounds(490, 240, 100, 29);

        jLabel11.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel11.setText("Fecha I:");
        panelReflect1.add(jLabel11);
        jLabel11.setBounds(30, 290, 80, 16);

        txt_observaciones.setColumns(20);
        txt_observaciones.setLineWrap(true);
        txt_observaciones.setRows(5);
        txt_observaciones.setWrapStyleWord(true);
        jScrollPane2.setViewportView(txt_observaciones);

        panelReflect1.add(jScrollPane2);
        jScrollPane2.setBounds(110, 330, 210, 60);

        jLabel12.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel12.setText("Precio:");
        panelReflect1.add(jLabel12);
        jLabel12.setBounds(340, 400, 80, 16);
        panelReflect1.add(txt_fechaI);
        txt_fechaI.setBounds(110, 290, 210, 28);

        cbx_duracion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "3 meses", "6 meses", "9 meses", "12 meses", "15 meses", "18 meses", "21 meses", "24 meses" }));
        panelReflect1.add(cbx_duracion);
        cbx_duracion.setBounds(420, 290, 170, 30);
        panelReflect1.add(txt_cedula);
        txt_cedula.setBounds(110, 130, 390, 30);

        txt_contrato.setEnabled(false);
        panelReflect1.add(txt_contrato);
        txt_contrato.setBounds(110, 400, 130, 30);

        txt_codigo.setEnabled(false);
        panelReflect1.add(txt_codigo);
        txt_codigo.setBounds(390, 70, 200, 30);
        panelReflect1.add(txt_precio);
        txt_precio.setBounds(430, 400, 160, 30);

        txt_cliente.setEnabled(false);
        panelReflect1.add(txt_cliente);
        txt_cliente.setBounds(110, 170, 210, 30);
        panelReflect1.add(txt_garantia);
        txt_garantia.setBounds(430, 350, 160, 30);

        jLabel13.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel13.setText("Garantia:");
        panelReflect1.add(jLabel13);
        jLabel13.setBounds(340, 350, 80, 16);

        jPanel2.add(panelReflect1);
        panelReflect1.setBounds(10, 340, 620, 490);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel5.setText("Buscar:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(0, 20, 80, 16);
        jPanel1.add(txt_busqueda);
        txt_busqueda.setBounds(70, 10, 230, 30);

        radio_vigente.setText("Vigentes");
        radio_vigente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio_vigenteItemStateChanged(evt);
            }
        });
        jPanel1.add(radio_vigente);
        radio_vigente.setBounds(400, 10, 110, 23);

        raio_caducados.setText("Caducados");
        raio_caducados.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                raio_caducadosItemStateChanged(evt);
            }
        });
        jPanel1.add(raio_caducados);
        raio_caducados.setBounds(500, 10, 110, 23);

        radio_todos.setSelected(true);
        radio_todos.setText("Todos");
        radio_todos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio_todosItemStateChanged(evt);
            }
        });
        jPanel1.add(radio_todos);
        radio_todos.setBounds(310, 10, 80, 23);

        jPanel2.add(jPanel1);
        jPanel1.setBounds(20, 80, 620, 50);

        labelCustom2.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom2.setText("ADMINISTRAR CONTRATOS");
        jPanel2.add(labelCustom2);
        labelCustom2.setBounds(10, 10, 630, 50);

        buttonAeroRight6.setBackground(new java.awt.Color(229, 14, 14));
        buttonAeroRight6.setText("Nuevo");
        jPanel2.add(buttonAeroRight6);
        buttonAeroRight6.setBounds(300, 290, 120, 29);

        buttonAeroLeft2.setBackground(new java.awt.Color(31, 157, 76));
        buttonAeroLeft2.setText("Editar");
        buttonAeroLeft2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroLeft2ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonAeroLeft2);
        buttonAeroLeft2.setBounds(180, 290, 110, 29);

        buttonCircle1.setText("buttonCircle1");
        buttonCircle1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCircle1ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonCircle1);
        buttonCircle1.setBounds(570, 290, 46, 40);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 650, 840);

        setSize(new java.awt.Dimension(659, 869));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonCircle1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCircle1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_buttonCircle1ActionPerformed

    private void buttonAeroRight5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight5ActionPerformed
        // TODO add your handling code here:
        InmuebleService aux = new InmuebleService();
        aux.fijarInmueble(UtilidadesConponente.obtenerInmueble(cbx_inmueble));
        new Frm_MostrarDatosInmueble(null, true, aux).setVisible(true);
    }//GEN-LAST:event_buttonAeroRight5ActionPerformed

    private void buttonAeroRight1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight1ActionPerformed
        // TODO add your handling code here:
        buscarPersona();
    }//GEN-LAST:event_buttonAeroRight1ActionPerformed

    private void cbx_inmuebleItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbx_inmuebleItemStateChanged
        // TODO add your handling code here:
        cargarPrecio();
    }//GEN-LAST:event_cbx_inmuebleItemStateChanged

    private void buttonAeroRight4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight4ActionPerformed
        // TODO add your handling code here:
        cargarFileChooser();
    }//GEN-LAST:event_buttonAeroRight4ActionPerformed

    private void buttonAeroLeft1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroLeft1ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_buttonAeroLeft1ActionPerformed

    private void buttonAeroLeft2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroLeft2ActionPerformed
        // TODO add your handling code here:
        editar();
    }//GEN-LAST:event_buttonAeroLeft2ActionPerformed

    private void radio_todosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio_todosItemStateChanged
        // TODO add your handling code here:
        filtrar();
    }//GEN-LAST:event_radio_todosItemStateChanged

    private void radio_vigenteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio_vigenteItemStateChanged
        // TODO add your handling code here:
        filtrar();
    }//GEN-LAST:event_radio_vigenteItemStateChanged

    private void raio_caducadosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_raio_caducadosItemStateChanged
        // TODO add your handling code here:
        filtrar();
    }//GEN-LAST:event_raio_caducadosItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmContrato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmContrato dialog = new FrmContrato(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonAeroLeft buttonAeroLeft1;
    private org.edisoncor.gui.button.ButtonAeroLeft buttonAeroLeft2;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight1;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight2;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight4;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight5;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight6;
    private org.edisoncor.gui.button.ButtonCircle buttonCircle1;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbx_duracion;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbx_inmueble;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private org.edisoncor.gui.label.LabelCustom labelCustom1;
    private org.edisoncor.gui.label.LabelCustom labelCustom2;
    private org.edisoncor.gui.panel.PanelReflect panelReflect1;
    private javax.swing.JRadioButton radio_todos;
    private javax.swing.JRadioButton radio_vigente;
    private javax.swing.JRadioButton raio_caducados;
    private javax.swing.JTable tbl_tabla;
    private org.edisoncor.gui.textField.TextFieldRound txt_busqueda;
    private org.edisoncor.gui.textField.TextFieldRound txt_cedula;
    private org.edisoncor.gui.textField.TextFieldRound txt_cliente;
    private org.edisoncor.gui.textField.TextFieldRound txt_codigo;
    private org.edisoncor.gui.textField.TextFieldRound txt_contrato;
    private com.toedter.calendar.JDateChooser txt_fechaI;
    private org.edisoncor.gui.textField.TextFieldRound txt_fono;
    private org.edisoncor.gui.textField.TextFieldRound txt_garantia;
    private javax.swing.JTextArea txt_observaciones;
    private org.edisoncor.gui.textField.TextFieldRound txt_precio;
    // End of variables declaration//GEN-END:variables
}
