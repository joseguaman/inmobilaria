/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.reportes.ManejadorReporte;
import controlador.servicios.InmuebleService;
import controlador.servicios.PagoService;
import controlador.servicios.PersonaService;
import controlador.utilidades.Utilidades;
import java.util.Date;
import java.util.stream.Collectors;
import vista.tablas.DataPago;
import vista.tablas.ModeloTablaInmueble;
import vista.tablas.ModeloTablaPago;
import vista.tablas.NodeloTablaPersona;
import vista.utilidades.UtilidadesConponente;

/**
 *
 * @author sissysebas
 */
public class Frm_reportes extends javax.swing.JDialog {
    private ModeloTablaPago modeloP = new ModeloTablaPago();
    private PagoService pagoService = new PagoService();
    
    private NodeloTablaPersona modeloCliente = new NodeloTablaPersona();
    private PersonaService personaService = new PersonaService();


    private NodeloTablaPersona modeloPropietario = new NodeloTablaPersona();
    private PersonaService propetarioService = new PersonaService();    
    
    private ModeloTablaInmueble modeloTablaInmueble = new ModeloTablaInmueble();
    private InmuebleService inmuebleService = new InmuebleService();
    
    private ModeloTablaPago modeloPI = new ModeloTablaPago();
    /**
     * Creates new form Frm_reportes
     */
    public Frm_reportes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        fecha_fin.setDate(new Date());
        fecha_ini.setDate(new Date());
        cargarTablaPgo();
        cargarTablaCliente();
        cargarTablaPropetario();
        cargarComboPorpietarios();
        cargarInmueblePropietario();
        cargarComboPropietario();
        cargarComboInmueblePropietario();
        pagosInmuebleCliente();
    }
    
    private void cargarComboPorpietarios() {
        UtilidadesConponente.llenarComboPropietarios(personaService, cbxpropietario);        
    }
    
    private void cargarInmueblePropietario() {
        if(radioinmueble.isSelected()) {
            modeloTablaInmueble.setLista(inmuebleService.todos());
        } else {
            modeloTablaInmueble.setLista(inmuebleService.listarInmueblePropietario(UtilidadesConponente.obtenerInmueble(cbxpropietario).getPersona().getExternal_id()));
        }
        
        tbltablainmueble.setModel(modeloTablaInmueble);
        tbltablainmueble.updateUI();
        if (!modeloTablaInmueble.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablainmueble);
            }
    }
    
    private void cargarTablaPropetario() {
        modeloPropietario.setLista(propetarioService.listarSinAdministradorTipo("Propietario"));
        tbltablapropietario.setModel(modeloPropietario);
        tbltablapropietario.updateUI();
        if (!modeloPropietario.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablapropietario);
            }
    }
    
    private void cargarTablaCliente() {
        modeloCliente.setLista(personaService.listarSinAdministradorTipo("Arrendatario"));
        tbltablaCliente.setModel(modeloCliente);
        tbltablaCliente.updateUI();
        if (!modeloCliente.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablaP);
            }
    }
    
    private void cargarTablaPgo() {
        modeloP.setLista(new DataPago().cargarListaPagado());
        tbltablaP.setModel(modeloP);
        tbltablaP.updateUI();
        if (!modeloP.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablaP);
            }
    }
    
    private void filtrarPago() {
        txtbusquedaP.setText("");
        cargarTablaPgo();
    }

    private void buscarPago() {
        String texto = txtbusquedaP.getText();
        if(cbxbusquedaP.getSelectedIndex() == 0) {
            modeloP.setLista(modeloP.getLista().stream()
                                .filter(t -> {
                                    if(t.cliente.toLowerCase().contains(texto.toLowerCase()) 
                                            || t.cedula.toLowerCase().contains(texto.toLowerCase())
                                            || t.nroContrato.toLowerCase().contains(texto.toLowerCase()))
                                        return true;
                                     else 
                                        return false;
                                }).collect(Collectors.toList()));
        } else if(cbxbusquedaP.getSelectedIndex() == 1) {
            modeloP.setLista(modeloP.getLista().stream()
                                .filter(t -> {
                                    if(t.propietario.toLowerCase().contains(texto.toLowerCase()) )
                                        return true;
                                     else 
                                        return false;
                                }).collect(Collectors.toList()));
        } else if(cbxbusquedaP.getSelectedIndex() == 2) {
            modeloP.setLista(modeloP.getLista().stream()
                                .filter(t -> {
                                    if(t.nroContrato.toLowerCase().contains(texto.toLowerCase()) )
                                        return true;
                                     else 
                                        return false;
                                }).collect(Collectors.toList()));
        } else {
            modeloP.setLista(modeloP.getLista().stream()
                                .filter(t -> {
                                    if(t.cliente.toLowerCase().contains(texto.toLowerCase()) )
                                        return true;
                                     else 
                                        return false;
                                }).collect(Collectors.toList()));
        }
        if (modeloP.getLista().isEmpty()) {
            UtilidadesConponente.mensajeError("Error", "No existe resultados para esta busqueda");
            cargarTablaPgo();
        } else {
            tbltablaP.setModel(modeloP);
            tbltablaP.updateUI();
            if (!modeloP.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablaP);
            }
        }
    }
    private void cargarComboPropietario() {
        UtilidadesConponente.llenarComboPropietarios(personaService, cbxcombopropietarioP);
    }
    private void cargarComboInmueblePropietario() {
        if(cbxcombopropietarioP.getItemCount() > 0) {
            UtilidadesConponente.cargarComboInmueblePr(cbxcomboinmueblep, UtilidadesConponente.obtenerPersonaCombo(cbxcombopropietarioP));
        }
    }
    private void pagosInmuebleCliente() {
        try {
            modeloPI.setLista(new DataPago().cargarListaPagado(pagoService.listarPagosPagadosInmueble(false, UtilidadesConponente.obtenerInmueble(cbxcomboinmueblep).getCodigo(), fecha_ini.getDate(), fecha_fin.getDate())));
        tbltablapagosinmueble.setModel(modeloPI);
        tbltablapagosinmueble.updateUI();
        if (!modeloPI.getLista().isEmpty()) {
                UtilidadesConponente.tablePack(tbltablapagosinmueble);
            }
            System.out.println("Entrre a buscar pagos");
        } catch (Exception e) {
            System.out.println("error "+e);
        }
        
        
        //listarPagosPagadosInmueble
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tabbedSelector21 = new org.edisoncor.gui.tabbedPane.TabbedSelector2();
        jPanel2 = new javax.swing.JPanel();
        panelReflect1 = new org.edisoncor.gui.panel.PanelReflect();
        jLabel1 = new javax.swing.JLabel();
        cbxbusquedaP = new org.edisoncor.gui.comboBox.ComboBoxRect();
        txtbusquedaP = new org.edisoncor.gui.textField.TextFieldRound();
        btnbuscarP = new org.edisoncor.gui.button.ButtonAeroRight();
        panelReflect2 = new org.edisoncor.gui.panel.PanelReflect();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbltablaP = new javax.swing.JTable();
        btnimpirmirP = new org.edisoncor.gui.button.ButtonAeroRight();
        jPanel4 = new javax.swing.JPanel();
        panelReflect5 = new org.edisoncor.gui.panel.PanelReflect();
        jLabel3 = new javax.swing.JLabel();
        comboBoxRect3 = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jTextField2 = new javax.swing.JTextField();
        panelReflect6 = new org.edisoncor.gui.panel.PanelReflect();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbltablaCliente = new javax.swing.JTable();
        buttonAeroRight3 = new org.edisoncor.gui.button.ButtonAeroRight();
        jPanel5 = new javax.swing.JPanel();
        panelReflect7 = new org.edisoncor.gui.panel.PanelReflect();
        jLabel4 = new javax.swing.JLabel();
        cbxpropietario = new org.edisoncor.gui.comboBox.ComboBoxRect();
        radioinmueble = new javax.swing.JRadioButton();
        panelReflect8 = new org.edisoncor.gui.panel.PanelReflect();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbltablainmueble = new javax.swing.JTable();
        buttonAeroRight4 = new org.edisoncor.gui.button.ButtonAeroRight();
        jPanel6 = new javax.swing.JPanel();
        panelReflect9 = new org.edisoncor.gui.panel.PanelReflect();
        jLabel5 = new javax.swing.JLabel();
        comboBoxRect5 = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jTextField3 = new javax.swing.JTextField();
        panelReflect10 = new org.edisoncor.gui.panel.PanelReflect();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbltablapropietario = new javax.swing.JTable();
        buttonAeroRight5 = new org.edisoncor.gui.button.ButtonAeroRight();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        fecha_fin = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        fecha_ini = new com.toedter.calendar.JDateChooser();
        cbxcomboinmueblep = new org.edisoncor.gui.comboBox.ComboBoxRect();
        cbxcombopropietarioP = new org.edisoncor.gui.comboBox.ComboBoxRect();
        btnbuscarP1 = new org.edisoncor.gui.button.ButtonAeroRight();
        buttonAeroRight6 = new org.edisoncor.gui.button.ButtonAeroRight();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbltablapagosinmueble = new javax.swing.JTable();
        labelCustom2 = new org.edisoncor.gui.label.LabelCustom();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setLayout(null);

        panelReflect1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel1.setText("Buscar por:");
        panelReflect1.add(jLabel1);
        jLabel1.setBounds(10, 20, 80, 16);

        cbxbusquedaP.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todos", "Propietario", "Nro Contrato", "Arrendatario" }));
        cbxbusquedaP.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxbusquedaPItemStateChanged(evt);
            }
        });
        panelReflect1.add(cbxbusquedaP);
        cbxbusquedaP.setBounds(100, 20, 110, 20);
        panelReflect1.add(txtbusquedaP);
        txtbusquedaP.setBounds(230, 20, 370, 30);

        btnbuscarP.setBackground(new java.awt.Color(128, 165, 87));
        btnbuscarP.setText("BUSCAR");
        btnbuscarP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarPActionPerformed(evt);
            }
        });
        panelReflect1.add(btnbuscarP);
        btnbuscarP.setBounds(610, 20, 90, 30);

        jPanel2.add(panelReflect1);
        panelReflect1.setBounds(10, 10, 710, 70);

        panelReflect2.setLayout(null);

        tbltablaP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbltablaP);

        panelReflect2.add(jScrollPane1);
        jScrollPane1.setBounds(10, 10, 700, 160);

        btnimpirmirP.setBackground(new java.awt.Color(128, 165, 87));
        btnimpirmirP.setText("IMPRIMIR");
        btnimpirmirP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimpirmirPActionPerformed(evt);
            }
        });
        panelReflect2.add(btnimpirmirP);
        btnimpirmirP.setBounds(590, 180, 79, 29);

        jPanel2.add(panelReflect2);
        panelReflect2.setBounds(10, 90, 710, 220);

        tabbedSelector21.addTab("PAGOS", jPanel2);

        jPanel4.setLayout(null);

        panelReflect5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect5.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel3.setText("Buscar por:");
        panelReflect5.add(jLabel3);
        jLabel3.setBounds(10, 20, 80, 16);

        comboBoxRect3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cedula", "Apellidos" }));
        panelReflect5.add(comboBoxRect3);
        comboBoxRect3.setBounds(100, 20, 110, 20);
        panelReflect5.add(jTextField2);
        jTextField2.setBounds(230, 20, 470, 28);

        jPanel4.add(panelReflect5);
        panelReflect5.setBounds(10, 10, 710, 70);

        panelReflect6.setLayout(null);

        tbltablaCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbltablaCliente);

        panelReflect6.add(jScrollPane3);
        jScrollPane3.setBounds(10, 10, 700, 160);

        buttonAeroRight3.setBackground(new java.awt.Color(128, 165, 87));
        buttonAeroRight3.setText("IMPRIMIR");
        buttonAeroRight3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight3ActionPerformed(evt);
            }
        });
        panelReflect6.add(buttonAeroRight3);
        buttonAeroRight3.setBounds(570, 180, 79, 29);

        jPanel4.add(panelReflect6);
        panelReflect6.setBounds(10, 90, 710, 220);

        tabbedSelector21.addTab("CLIENTES", jPanel4);

        jPanel5.setLayout(null);

        panelReflect7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect7.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel4.setText("Propietario:");
        panelReflect7.add(jLabel4);
        jLabel4.setBounds(10, 20, 100, 16);

        panelReflect7.add(cbxpropietario);
        cbxpropietario.setBounds(130, 20, 320, 20);

        radioinmueble.setSelected(true);
        radioinmueble.setText("Todos");
        panelReflect7.add(radioinmueble);
        radioinmueble.setBounds(480, 20, 68, 23);

        jPanel5.add(panelReflect7);
        panelReflect7.setBounds(10, 10, 710, 70);

        panelReflect8.setLayout(null);

        tbltablainmueble.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tbltablainmueble);

        panelReflect8.add(jScrollPane4);
        jScrollPane4.setBounds(10, 10, 700, 160);

        buttonAeroRight4.setBackground(new java.awt.Color(128, 165, 87));
        buttonAeroRight4.setText("IMPRIMIR");
        buttonAeroRight4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight4ActionPerformed(evt);
            }
        });
        panelReflect8.add(buttonAeroRight4);
        buttonAeroRight4.setBounds(570, 180, 79, 29);

        jPanel5.add(panelReflect8);
        panelReflect8.setBounds(10, 90, 710, 220);

        tabbedSelector21.addTab("INMUEBLES", jPanel5);

        jPanel6.setLayout(null);

        panelReflect9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect9.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel5.setText("Buscar por:");
        panelReflect9.add(jLabel5);
        jLabel5.setBounds(10, 20, 80, 16);

        comboBoxRect5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cedula", "Apellidos" }));
        panelReflect9.add(comboBoxRect5);
        comboBoxRect5.setBounds(100, 20, 110, 20);
        panelReflect9.add(jTextField3);
        jTextField3.setBounds(230, 20, 470, 28);

        jPanel6.add(panelReflect9);
        panelReflect9.setBounds(10, 10, 710, 70);

        panelReflect10.setLayout(null);

        tbltablapropietario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tbltablapropietario);

        panelReflect10.add(jScrollPane5);
        jScrollPane5.setBounds(10, 10, 700, 160);

        buttonAeroRight5.setBackground(new java.awt.Color(128, 165, 87));
        buttonAeroRight5.setText("IMPRIMIR");
        buttonAeroRight5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight5ActionPerformed(evt);
            }
        });
        panelReflect10.add(buttonAeroRight5);
        buttonAeroRight5.setBounds(570, 180, 79, 29);

        jPanel6.add(panelReflect10);
        panelReflect10.setBounds(10, 90, 710, 220);

        tabbedSelector21.addTab("PROPIETARIOS", jPanel6);

        jPanel3.setLayout(null);

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(null);

        jLabel6.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel6.setText("Propietario:");
        jPanel7.add(jLabel6);
        jLabel6.setBounds(10, 20, 100, 16);

        jLabel7.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel7.setText("Fecha finalizacion:");
        jPanel7.add(jLabel7);
        jLabel7.setBounds(370, 60, 150, 16);

        jLabel8.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel8.setText("Inmueble:");
        jPanel7.add(jLabel8);
        jLabel8.setBounds(370, 20, 100, 16);

        fecha_fin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fecha_finMouseClicked(evt);
            }
        });
        jPanel7.add(fecha_fin);
        fecha_fin.setBounds(510, 50, 200, 28);

        jLabel9.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel9.setText("Fecha Inicio:");
        jPanel7.add(jLabel9);
        jLabel9.setBounds(10, 60, 110, 16);

        fecha_ini.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fecha_iniMouseClicked(evt);
            }
        });
        jPanel7.add(fecha_ini);
        fecha_ini.setBounds(120, 50, 240, 28);

        cbxcomboinmueblep.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxcomboinmueblepItemStateChanged(evt);
            }
        });
        jPanel7.add(cbxcomboinmueblep);
        cbxcomboinmueblep.setBounds(510, 20, 200, 20);

        cbxcombopropietarioP.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxcombopropietarioPItemStateChanged(evt);
            }
        });
        jPanel7.add(cbxcombopropietarioP);
        cbxcombopropietarioP.setBounds(120, 10, 210, 20);

        btnbuscarP1.setBackground(new java.awt.Color(128, 165, 87));
        btnbuscarP1.setText("BUSCAR");
        btnbuscarP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarP1ActionPerformed(evt);
            }
        });
        jPanel7.add(btnbuscarP1);
        btnbuscarP1.setBounds(610, 90, 90, 30);

        jPanel3.add(jPanel7);
        jPanel7.setBounds(10, 10, 720, 130);

        buttonAeroRight6.setBackground(new java.awt.Color(128, 165, 87));
        buttonAeroRight6.setText("IMPRIMIR");
        buttonAeroRight6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight6ActionPerformed(evt);
            }
        });
        jPanel3.add(buttonAeroRight6);
        buttonAeroRight6.setBounds(630, 270, 79, 29);

        tbltablapagosinmueble.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbltablapagosinmueble);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(10, 150, 720, 110);

        tabbedSelector21.addTab("PAGOS INMUEBLES", jPanel3);

        jPanel1.add(tabbedSelector21);
        tabbedSelector21.setBounds(10, 70, 920, 330);

        labelCustom2.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom2.setText("REPORTES");
        jPanel1.add(labelCustom2);
        labelCustom2.setBounds(10, 10, 920, 50);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 950, 430);

        setSize(new java.awt.Dimension(958, 456));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnimpirmirPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimpirmirPActionPerformed
        // TODO add your handling code here:
        int fila = tbltablaP.getSelectedRow();
        if (fila >= 0) {
            DataPago pago = modeloP.getLista().get(fila);
            ManejadorReporte.crearReportePagos(pago);
        } else {
            UtilidadesConponente.mensajeError("Error", "Seleccione un pago");
        }
    }//GEN-LAST:event_btnimpirmirPActionPerformed

    private void btnbuscarPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarPActionPerformed
        // TODO add your handling code here:
        buscarPago();
    }//GEN-LAST:event_btnbuscarPActionPerformed

    private void cbxbusquedaPItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxbusquedaPItemStateChanged
        // TODO add your handling code here:
        filtrarPago();
    }//GEN-LAST:event_cbxbusquedaPItemStateChanged

    private void buttonAeroRight5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight5ActionPerformed
        // TODO add your handling code here:
        ManejadorReporte.reportePropietarios(modeloPropietario.getLista());
    }//GEN-LAST:event_buttonAeroRight5ActionPerformed

    private void buttonAeroRight3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight3ActionPerformed
        // TODO add your handling code here:
        ManejadorReporte.reporteCliente(modeloCliente.getLista());
    }//GEN-LAST:event_buttonAeroRight3ActionPerformed

    private void buttonAeroRight4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight4ActionPerformed
        // TODO add your handling code here:
        ManejadorReporte.reporteInmueble(modeloTablaInmueble.getLista());
    }//GEN-LAST:event_buttonAeroRight4ActionPerformed

    private void buttonAeroRight6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight6ActionPerformed
        // TODO add your handling code here:
        if(!modeloPI.getLista().isEmpty()) {
            ManejadorReporte.crearReportePagosInmueble(modeloPI.getLista(), UtilidadesConponente.obtenerPersonaCombo(cbxcombopropietarioP).toString(), Utilidades.formatearFechaSimple(fecha_ini.getDate()), Utilidades.formatearFechaSimple(fecha_fin.getDate()));
        }
    }//GEN-LAST:event_buttonAeroRight6ActionPerformed

    private void cbxcombopropietarioPItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxcombopropietarioPItemStateChanged
        // TODO add your handling code here:
        this.cargarComboInmueblePropietario();
    }//GEN-LAST:event_cbxcombopropietarioPItemStateChanged

    private void cbxcomboinmueblepItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxcomboinmueblepItemStateChanged
        // TODO add your handling code here:
        pagosInmuebleCliente();
    }//GEN-LAST:event_cbxcomboinmueblepItemStateChanged

    private void fecha_iniMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fecha_iniMouseClicked
        // TODO add your handling code here:
        pagosInmuebleCliente();
    }//GEN-LAST:event_fecha_iniMouseClicked

    private void fecha_finMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fecha_finMouseClicked
        // TODO add your handling code here:
        pagosInmuebleCliente();
    }//GEN-LAST:event_fecha_finMouseClicked

    private void btnbuscarP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarP1ActionPerformed
        // TODO add your handling code here:
        pagosInmuebleCliente();
    }//GEN-LAST:event_btnbuscarP1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Frm_reportes dialog = new Frm_reportes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonAeroRight btnbuscarP;
    private org.edisoncor.gui.button.ButtonAeroRight btnbuscarP1;
    private org.edisoncor.gui.button.ButtonAeroRight btnimpirmirP;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight3;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight4;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight5;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight6;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbxbusquedaP;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbxcomboinmueblep;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbxcombopropietarioP;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbxpropietario;
    private org.edisoncor.gui.comboBox.ComboBoxRect comboBoxRect3;
    private org.edisoncor.gui.comboBox.ComboBoxRect comboBoxRect5;
    private com.toedter.calendar.JDateChooser fecha_fin;
    private com.toedter.calendar.JDateChooser fecha_ini;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private org.edisoncor.gui.label.LabelCustom labelCustom2;
    private org.edisoncor.gui.panel.PanelReflect panelReflect1;
    private org.edisoncor.gui.panel.PanelReflect panelReflect10;
    private org.edisoncor.gui.panel.PanelReflect panelReflect2;
    private org.edisoncor.gui.panel.PanelReflect panelReflect5;
    private org.edisoncor.gui.panel.PanelReflect panelReflect6;
    private org.edisoncor.gui.panel.PanelReflect panelReflect7;
    private org.edisoncor.gui.panel.PanelReflect panelReflect8;
    private org.edisoncor.gui.panel.PanelReflect panelReflect9;
    private javax.swing.JRadioButton radioinmueble;
    private org.edisoncor.gui.tabbedPane.TabbedSelector2 tabbedSelector21;
    private javax.swing.JTable tbltablaCliente;
    private javax.swing.JTable tbltablaP;
    private javax.swing.JTable tbltablainmueble;
    private javax.swing.JTable tbltablapagosinmueble;
    private javax.swing.JTable tbltablapropietario;
    private org.edisoncor.gui.textField.TextFieldRound txtbusquedaP;
    // End of variables declaration//GEN-END:variables
}
