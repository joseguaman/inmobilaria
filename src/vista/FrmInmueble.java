/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.servicios.InmuebleService;
import controlador.servicios.PersonaService;
import javax.swing.JOptionPane;
import vista.tablas.ModeloTablaInmueble;
import vista.utilidades.UtilidadesConponente;
import vista.utilidades.validadores.EventoTecladoLetrasNumeros;
import vista.utilidades.validadores.EventoTecladoNumeros;

/**
 *
 * @author sissysebas
 */
public class FrmInmueble extends javax.swing.JDialog {
    private InmuebleService is = new InmuebleService();
    private PersonaService ps = new PersonaService();
    
    private ModeloTablaInmueble modelo  = new ModeloTablaInmueble();
    /**
     * Creates new form FrmCliente
     */
    public FrmInmueble(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        //buttonGroup1.add(raio_cliente1);
        //buttonGroup1.add(radio_propietario1);
        limpiar();
        txt_barrio.addKeyListener(new EventoTecladoLetrasNumeros(txt_barrio));
        txt_calle.addKeyListener(new EventoTecladoLetrasNumeros(txt_calle));
        txt_calleS.addKeyListener(new EventoTecladoLetrasNumeros(txt_calleS));
        txt_ciudad.addKeyListener(new EventoTecladoLetrasNumeros(txt_ciudad));
        txt_nroHab.addKeyListener(new EventoTecladoNumeros(txt_nroHab));
        txt_pisos.addKeyListener(new EventoTecladoNumeros(txt_pisos));
        txt_precio.addKeyListener(new EventoTecladoNumeros(txt_precio));
    }
    
    private void cargarTabla() {
        modelo.setLista(is.todos());
        tbl_tabla.setModel(modelo);
        tbl_tabla.updateUI();
    }
    
    private void buscra() {
        if(txt_buxcar.getText().trim().length() >= 3) {
            modelo.setLista(is.listarLikeInmueble(txt_buxcar.getText()));
        tbl_tabla.setModel(modelo);
        tbl_tabla.updateUI();
        } else {
            cargarTabla();
        }
    }
    
    private void codigoInmueble() {
        String tipo = cbx_tipo.getSelectedItem().toString().toUpperCase()+"-";
        for(int i = 0; i < 3; i++) {
            tipo+= "0";
        }
        tipo += (is.todos().size()+1);
        txt_codigo.setText(tipo);
    }
    
    private void limpiar() {
        UtilidadesConponente.llenarComboPropietarios(ps, cbx_propietario);
        cargarTabla();
        codigoInmueble();
        txt_barrio.setText("");
        txt_calle.setText("");
        txt_calleS.setText("");
        txt_ciudad.setText("");
        txt_pisos.setText("0");
        txt_nroHab.setText("");
        txt_precio.setText("0.0");
       txt_codigo.setEnabled(false);
       is.fijarInmueble(null);
       ps.fijarPersona(null);
    }
    
    private void cargarObjeto() {
        is.getInmueble().setBarrio(txt_barrio.getText());
        is.getInmueble().setCallePrincipal(txt_calle.getText());
        is.getInmueble().setCalleSecundaria(txt_calleS.getText());
        is.getInmueble().setCodigo(txt_codigo.getText());
        is.getInmueble().setCiudad(txt_ciudad.getText());
        
        is.getInmueble().setNroHabitaciones(Integer.parseInt(txt_nroHab.getText()));
        is.getInmueble().setNroPisos(Integer.parseInt(txt_pisos.getText()));
        is.getInmueble().setPrecio(Double.parseDouble(txt_precio.getText()));
        is.getInmueble().setTipo(cbx_tipo.getSelectedItem().toString());
        is.getInmueble().setPersona(UtilidadesConponente.obtenerPersonaCombo(cbx_propietario));
    }
    
    private void cargarVista() {
        int fila = tbl_tabla.getSelectedRow();
        if(fila >= 0 ) {
            is.fijarInmueble(modelo.getLista().get(fila));
            txt_barrio.setText(is.getInmueble().getBarrio());
            txt_calle.setText(is.getInmueble().getCallePrincipal());
            txt_calleS.setText(is.getInmueble().getCalleSecundaria());
            txt_ciudad.setText(is.getInmueble().getCiudad());
            txt_codigo.setText(is.getInmueble().getCodigo());
            txt_nroHab.setText(String.valueOf(is.getInmueble().getNroHabitaciones()));
            txt_pisos.setText(String.valueOf(is.getInmueble().getNroPisos()));
            txt_precio.setText(String.valueOf(is.getInmueble().getPrecio()));
            cbx_tipo.setSelectedItem(is.getInmueble().getTipo());
            cbx_propietario.setSelectedItem(is.getInmueble().getPersona());
        } else {
            UtilidadesConponente.mensajeError("Error", "Llene todoss los campos");
        }
    }
    
    private void guardar() {
        String mensaje = "Se requiere este campo";
        if(!UtilidadesConponente.mostrarError(txt_barrio, mensaje, 'r') &&
                !UtilidadesConponente.mostrarError(txt_calle, mensaje, 'r') && 
                !UtilidadesConponente.mostrarError(txt_calleS, mensaje, 'r') && 
                !UtilidadesConponente.mostrarError(txt_ciudad, mensaje, 'r') && 
                !UtilidadesConponente.mostrarError(txt_codigo, mensaje, 'r') && 
                !UtilidadesConponente.mostrarError(txt_nroHab, mensaje, 'r') &&
                !UtilidadesConponente.mostrarError(txt_pisos, mensaje, 'r') &&
                !UtilidadesConponente.mostrarError(txt_precio, mensaje, 'r'))  {
            cargarObjeto();
            if(is.getInmueble().getId() != null) {
                is.getInmueble().setEstado(false);
                if(is.guardar()) {
                    UtilidadesConponente.mensajeOk("Se ha guardado correctamente", mensaje);
                    limpiar();
                } else {
                    UtilidadesConponente.mensajeError("Error", "No se pudo guardar");
                }
            } else {
                if(is.guardar()) {
                    UtilidadesConponente.mensajeOk("OK", "Se ha modificado correctamente");
                    limpiar();
                } else {
                    UtilidadesConponente.mensajeError("Error", "No se pudo modificar");
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        panelReflect1 = new org.edisoncor.gui.panel.PanelReflect();
        buttonAeroLeft1 = new org.edisoncor.gui.button.ButtonAeroLeft();
        jLabel2 = new javax.swing.JLabel();
        txt_ciudad = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel4 = new javax.swing.JLabel();
        txt_barrio = new org.edisoncor.gui.textField.TextFieldRound();
        jLabel3 = new javax.swing.JLabel();
        buttonAeroRight2 = new org.edisoncor.gui.button.ButtonAeroRight();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        labelCustom1 = new org.edisoncor.gui.label.LabelCustom();
        labelCustom3 = new org.edisoncor.gui.label.LabelCustom();
        jLabel9 = new javax.swing.JLabel();
        cbx_propietario = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_calle = new org.edisoncor.gui.textField.TextFieldRound();
        txt_calleS = new org.edisoncor.gui.textField.TextFieldRound();
        txt_nroHab = new org.edisoncor.gui.textField.TextFieldRound();
        txt_codigo = new org.edisoncor.gui.textField.TextFieldRound();
        txt_precio = new org.edisoncor.gui.textField.TextFieldRound();
        txt_pisos = new org.edisoncor.gui.textField.TextFieldRound();
        cbx_tipo = new org.edisoncor.gui.comboBox.ComboBoxRect();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_buxcar = new org.edisoncor.gui.textField.TextFieldRound();
        buttonAeroRight1 = new org.edisoncor.gui.button.ButtonAeroRight();
        labelCustom2 = new org.edisoncor.gui.label.LabelCustom();
        buttonAeroLeft2 = new org.edisoncor.gui.button.ButtonAeroLeft();
        buttonAeroRight3 = new org.edisoncor.gui.button.ButtonAeroRight();
        buttonCircle1 = new org.edisoncor.gui.button.ButtonCircle();
        buttonRect1 = new org.edisoncor.gui.button.ButtonRect();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(null);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(20, 150, 710, 150);

        panelReflect1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReflect1.setLayout(null);

        buttonAeroLeft1.setBackground(new java.awt.Color(31, 157, 76));
        buttonAeroLeft1.setText("Guardar");
        buttonAeroLeft1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroLeft1ActionPerformed(evt);
            }
        });
        panelReflect1.add(buttonAeroLeft1);
        buttonAeroLeft1.setBounds(220, 360, 110, 29);

        jLabel2.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel2.setText("Ciudad:");
        panelReflect1.add(jLabel2);
        jLabel2.setBounds(30, 80, 80, 16);
        panelReflect1.add(txt_ciudad);
        txt_ciudad.setBounds(110, 70, 180, 30);

        jLabel4.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel4.setText("Barrio:");
        panelReflect1.add(jLabel4);
        jLabel4.setBounds(330, 80, 80, 16);
        panelReflect1.add(txt_barrio);
        txt_barrio.setBounds(410, 70, 180, 30);

        jLabel3.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel3.setText("Calle Se:");
        panelReflect1.add(jLabel3);
        jLabel3.setBounds(330, 110, 80, 16);

        buttonAeroRight2.setBackground(new java.awt.Color(229, 14, 14));
        buttonAeroRight2.setText("Cancelar");
        panelReflect1.add(buttonAeroRight2);
        buttonAeroRight2.setBounds(340, 360, 120, 29);

        jLabel6.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel6.setText("Calle Pr:");
        panelReflect1.add(jLabel6);
        jLabel6.setBounds(30, 110, 80, 16);

        jLabel7.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel7.setText("Codigo:");
        panelReflect1.add(jLabel7);
        jLabel7.setBounds(30, 210, 80, 16);

        jLabel8.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel8.setText("Nro Hab:");
        panelReflect1.add(jLabel8);
        jLabel8.setBounds(330, 210, 80, 16);

        labelCustom1.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom1.setText("DATOS GENERALES");
        panelReflect1.add(labelCustom1);
        labelCustom1.setBounds(10, 150, 690, 50);

        labelCustom3.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom3.setText("UBICACION");
        panelReflect1.add(labelCustom3);
        labelCustom3.setBounds(10, 10, 690, 50);

        jLabel9.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel9.setText("Precio:");
        panelReflect1.add(jLabel9);
        jLabel9.setBounds(320, 300, 80, 16);

        cbx_propietario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Departamento", "Casa", "Oficina", "Parcela", "Galpones", "Cuarto" }));
        panelReflect1.add(cbx_propietario);
        cbx_propietario.setBounds(410, 250, 180, 30);

        jLabel10.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel10.setText("Tipo:");
        panelReflect1.add(jLabel10);
        jLabel10.setBounds(30, 250, 80, 16);

        jLabel11.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel11.setText("Nro pisos:");
        panelReflect1.add(jLabel11);
        jLabel11.setBounds(30, 300, 80, 16);

        jLabel12.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel12.setText("Propieta:");
        panelReflect1.add(jLabel12);
        jLabel12.setBounds(320, 260, 80, 16);
        panelReflect1.add(txt_calle);
        txt_calle.setBounds(110, 110, 180, 30);
        panelReflect1.add(txt_calleS);
        txt_calleS.setBounds(410, 110, 180, 30);
        panelReflect1.add(txt_nroHab);
        txt_nroHab.setBounds(410, 210, 180, 30);
        panelReflect1.add(txt_codigo);
        txt_codigo.setBounds(110, 210, 180, 30);
        panelReflect1.add(txt_precio);
        txt_precio.setBounds(410, 290, 180, 30);
        panelReflect1.add(txt_pisos);
        txt_pisos.setBounds(110, 290, 180, 30);

        cbx_tipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Departamento", "Casa", "Oficina", "Parcela", "Galpones", "Cuarto" }));
        cbx_tipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbx_tipoItemStateChanged(evt);
            }
        });
        panelReflect1.add(cbx_tipo);
        cbx_tipo.setBounds(110, 250, 180, 30);

        jPanel2.add(panelReflect1);
        panelReflect1.setBounds(20, 360, 710, 400);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLabel5.setText("Buscar:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(20, 20, 80, 16);
        jPanel1.add(txt_buxcar);
        txt_buxcar.setBounds(90, 10, 420, 30);

        buttonAeroRight1.setBackground(new java.awt.Color(14, 62, 229));
        buttonAeroRight1.setText("BUSCAR");
        buttonAeroRight1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroRight1ActionPerformed(evt);
            }
        });
        jPanel1.add(buttonAeroRight1);
        buttonAeroRight1.setBounds(530, 10, 80, 29);

        jPanel2.add(jPanel1);
        jPanel1.setBounds(20, 80, 710, 50);

        labelCustom2.setBackground(new java.awt.Color(14, 4, 4));
        labelCustom2.setText("ADMINISTRAR INMUEBLE");
        jPanel2.add(labelCustom2);
        labelCustom2.setBounds(10, 10, 720, 50);

        buttonAeroLeft2.setBackground(new java.awt.Color(31, 157, 76));
        buttonAeroLeft2.setText("Editar");
        buttonAeroLeft2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAeroLeft2ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonAeroLeft2);
        buttonAeroLeft2.setBounds(200, 320, 110, 29);

        buttonAeroRight3.setBackground(new java.awt.Color(229, 14, 14));
        buttonAeroRight3.setText("Nuevo");
        jPanel2.add(buttonAeroRight3);
        buttonAeroRight3.setBounds(490, 320, 120, 29);

        buttonCircle1.setText("buttonCircle1");
        buttonCircle1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCircle1ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonCircle1);
        buttonCircle1.setBounds(670, 310, 46, 40);

        buttonRect1.setText("Act/Desc");
        buttonRect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRect1ActionPerformed(evt);
            }
        });
        jPanel2.add(buttonRect1);
        buttonRect1.setBounds(350, 320, 100, 29);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 740, 770);

        setSize(new java.awt.Dimension(751, 808));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonCircle1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCircle1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_buttonCircle1ActionPerformed

    private void cbx_tipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbx_tipoItemStateChanged
        // TODO add your handling code here:
        if(is.getInmueble().getId() == null) {
            codigoInmueble();
        }
    }//GEN-LAST:event_cbx_tipoItemStateChanged

    private void buttonAeroLeft1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroLeft1ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_buttonAeroLeft1ActionPerformed

    private void buttonAeroLeft2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroLeft2ActionPerformed
        // TODO add your handling code here:
        cargarVista();
    }//GEN-LAST:event_buttonAeroLeft2ActionPerformed

    private void buttonAeroRight1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAeroRight1ActionPerformed
        // TODO add your handling code here:
        buscra();
    }//GEN-LAST:event_buttonAeroRight1ActionPerformed

    private void buttonRect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRect1ActionPerformed
        // TODO add your handling code here:
        activar_desactivar();
    }//GEN-LAST:event_buttonRect1ActionPerformed
     private void activar_desactivar() {
         int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            InmuebleService p = new InmuebleService();
            p.fijarInmueble(modelo.getLista().get(fila));
            if(p.getInmueble().getActivo_desactivo() != null && p.getInmueble().getActivo_desactivo()) {
                int i = JOptionPane.showConfirmDialog(this, "Desea desactivar este inmueble?");
                if(i == JOptionPane.OK_OPTION) {
                    p.getInmueble().setActivo_desactivo(false);
                    p.guardar();
                    UtilidadesConponente.mensajeOk("OK", "Se ha desactivado correctamente");
                    limpiar();
                }
            } else {
                int i = JOptionPane.showConfirmDialog(this, "Desea activar este inmueble?");
                if(i == JOptionPane.OK_OPTION) {
                    p.getInmueble().setActivo_desactivo(true);
                    p.guardar();
                    UtilidadesConponente.mensajeOk("OK", "Se ha activado correctamente");
                    limpiar();
                }
            }
        } else {
            UtilidadesConponente.mensajeError("Error", "Escoja un dato de la tabla");
        }
     }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmInmueble dialog = new FrmInmueble(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonAeroLeft buttonAeroLeft1;
    private org.edisoncor.gui.button.ButtonAeroLeft buttonAeroLeft2;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight1;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight2;
    private org.edisoncor.gui.button.ButtonAeroRight buttonAeroRight3;
    private org.edisoncor.gui.button.ButtonCircle buttonCircle1;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.edisoncor.gui.button.ButtonRect buttonRect1;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbx_propietario;
    private org.edisoncor.gui.comboBox.ComboBoxRect cbx_tipo;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private org.edisoncor.gui.label.LabelCustom labelCustom1;
    private org.edisoncor.gui.label.LabelCustom labelCustom2;
    private org.edisoncor.gui.label.LabelCustom labelCustom3;
    private org.edisoncor.gui.panel.PanelReflect panelReflect1;
    private javax.swing.JTable tbl_tabla;
    private org.edisoncor.gui.textField.TextFieldRound txt_barrio;
    private org.edisoncor.gui.textField.TextFieldRound txt_buxcar;
    private org.edisoncor.gui.textField.TextFieldRound txt_calle;
    private org.edisoncor.gui.textField.TextFieldRound txt_calleS;
    private org.edisoncor.gui.textField.TextFieldRound txt_ciudad;
    private org.edisoncor.gui.textField.TextFieldRound txt_codigo;
    private org.edisoncor.gui.textField.TextFieldRound txt_nroHab;
    private org.edisoncor.gui.textField.TextFieldRound txt_pisos;
    private org.edisoncor.gui.textField.TextFieldRound txt_precio;
    // End of variables declaration//GEN-END:variables
}
